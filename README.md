# Small bookstore

Developed with Laravel it is an administrative system oriented to bookstores to manage their sales, it has a web portal for the public where they can find out the prices of products, FAQs, own business publications, etc.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes. 

### Prerequisites

You must have installed on your local machine:

* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [Docker](https://docs.docker.com/get-docker/)
* [Docker Compose](https://docs.docker.com/compose/install/)

### Installing

An advantage of docker is that with few commands you can build, configure and set up a project. Follow this steps to set up this project.


Download a copy of the project:

```
git clone https://gitlab.com/j_m_b/libreria_peq.git
```

Go into the project directory

```
cd libreria_peq
```

Initialize the project (this can take a while)

```
sudo docker-compose up --build -d
```

After you see this console message it means you are near to finnish the instalation

```
Creating app ... done
Creating webserver ... done
Creating db ... done
```




Now you must to run this comand to set up some laravel and enviroment requeriments

```
sudo chmod a+x easy_install.sh
sudo ./easy_install.sh
```

And thats all, you can access to this project through your [localhost](http://localhost/).

Initial acount:
```
Name: "admin"
Email: "admin@admin.com"
Password: "essau.co"
```

## Author

* **Jonathan Essau MB** - *Computer engineer* - [essau.co](https://essau.co/)
