<?php

use Illuminate\Database\Seeder;


use App\User;
use App\Models\Valoracion;
use App\Models\Categoria;
use App\Models\Autor;
use App\Models\Libro;




use Illuminate\Support\Facades\Hash;

use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Auth\RegistersUsers;

class DatabaseSeeder extends Seeder
{

    use RegistersUsers, HasRoles;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        // Check de que los registros no existan
        $super_admin_user = User::get();
        $roles = Role::get();
        $valoraciones = Valoracion::get();
        $categorias_blog = Categoria::get();
        $autores = Autor::get();
        $libros = Libro::get();


        if($super_admin_user->count() == 0  && $roles->count() == 0){
            //Creo los roles
            $role1 = Role::create([
                'name' => 'super-admin',
                'guard_name' => 'web'
                ]);
            $role2 = Role::create([
                'name' => 'admin',
                'guard_name' => 'web'
                ]);
            $role3 = Role::create([
                'name' => 'empleado',
                'guard_name' => 'web'
                ]);
            $role4 = Role::create([
                'name' => 'cliente',
                'guard_name' => 'web'
                ]);
    
            //Creado a admin
            $user = User::create([
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => Hash::make('essau.co'),
                ]);
            $user->assignRole('super-admin');
        }


        //creando valoraciones
        if ($valoraciones->count() == 0) {
            $valoracion1 = Valoracion::create(['valoracion' => 'Extraordinario']);
            $valoracion2 = Valoracion::create(['valoracion' => 'Excelente']);
            $valoracion3 = Valoracion::create(['valoracion' => 'Bueno']);
            $valoracion4 = Valoracion::create(['valoracion' => 'Dañado']);
        }

        //Creando catetorias para blog
        if ($categorias_blog->count() == 0) {
            $lista_categorias = array(
                array('categoria' => 'Tecnologia'),
                array('categoria' => 'Cine'),
                array('categoria' => 'Musica'),
                array('categoria' => 'Arte'),
                array('categoria' => 'Ciencia'),
                array('categoria' => 'Literatura'),
            );

            foreach ($lista_categorias as $data) {
                Categoria::create($data);
            }
        }

        //Creando algunos autores
        if($autores->count() == 0){
            $lista_autores = array(
                array(
                    'nombre' => 'Autoris',
                    'apellido' => 'Magistralis',
                    'fecha_nacimiento' => '1900-12-04',
                    'fecha_muerte' => '1914-12-04'
                ),
                array(
                    'nombre' => 'Harry',
                    'apellido' => 'Potter',
                    'fecha_nacimiento' => '1955-10-12',
                    'fecha_muerte' => '1999-12-31'
                ),
                array(
                    'nombre' => 'Arthas',
                    'apellido' => 'Menethil',
                    'fecha_nacimiento' => '1940-02-02',
                    'fecha_muerte' => '2010-04-04'
                ),
                array(
                    'nombre' => 'Frodo',
                    'apellido' => 'Bolson',
                    'fecha_nacimiento' => '1894-12-13',
                    'fecha_muerte' => '1928-09-01'
                ),
                array(
                    'nombre' => 'Anakin',
                    'apellido' => 'Skywalker',
                    'fecha_nacimiento' => '1985-11-26',
                ),
                array(
                    'nombre' => 'Indiana',
                    'apellido' => 'Jones',
                    'fecha_nacimiento' => '1935-08-04',
                ),
            );

            foreach ($lista_autores as $data) {
                Autor::create($data);
            }
        }

        //Creando algunos libros
        if ($libros->count() == 0) {
            $lista_libros = array(
                array(
                    'titulo' => 'Ensayo sobre la ceguera',
                    'editorial' => 'Editoriales ABC',
                    'fecha_publicacion' => '1999-12-12',
                    'precio_venta' => '20000',
                    'precio_minorista' => '15000',
                    'valoracion_id' => '2',
                    'descripcion_valoracion' => 'Pellentesque a ligula tristique, porttitor enim eget, rutrum augue. Fusce accumsan rutrum sollicitudin. Sed tempus a sapien id rhoncus. Donec vestibulum elit ut sem luctus dictum.',
                ),
                array(
                    'titulo' => 'Buscando la disrupcion',
                    'editorial' => 'Editoriales ABC',
                    'fecha_publicacion' => '2012-12-21',
                    'precio_venta' => '45000',
                    'precio_minorista' => '38000',
                    'valoracion_id' => '1',
                    'descripcion_valoracion' => 'Praesent molestie et elit vitae sagittis. Sed eu pharetra lectus, a viverra quam. Aliquam ultricies consequat magna sed porttitor.',
                ),
                array(
                    'titulo' => 'Cuentos para no dormir en Kalimdor',
                    'editorial' => 'Orgrimar e inscriptores',
                    'fecha_publicacion' => '2004-05-03',
                    'precio_venta' => '17000',
                    'precio_minorista' => '12000',
                    'valoracion_id' => '1',
                    'descripcion_valoracion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet ultricies ex id gravida. Quisque a tempus libero.',
                ),
                array(
                    'titulo' => 'Como ganarle al imperio y no morir en el intento',
                    'editorial' => 'Editoriales Yoda',
                    'fecha_publicacion' => '1999-12-12',
                    'precio_venta' => '20000',
                    'precio_minorista' => '15000',
                    'valoracion_id' => '2',
                    'descripcion_valoracion' => 'Pellentesque a ligula tristique, porttitor enim eget, rutrum augue. Fusce accumsan rutrum sollicitudin. Sed tempus a sapien id rhoncus. Donec vestibulum elit ut sem luctus dictum.',
                ),
                array(
                    'titulo' => 'Ciencia, tecnologia y mas tecnologia',
                    'editorial' => 'Essco Editoriales',
                    'fecha_publicacion' => '2013-01-08',
                    'precio_venta' => '150000',
                    'precio_minorista' => '115000',
                    'valoracion_id' => '1',
                    'descripcion_valoracion' => 'Aliquam ac lacinia orci. Suspendisse potenti. Aliquam id lorem lacus.',
                ),
                array(
                    'titulo' => 'Afuera en el espacio',
                    'editorial' => 'Esssco Editoriales',
                    'fecha_publicacion' => '1959-11-10',
                    'precio_venta' => '8500',
                    'precio_minorista' => '5000',
                    'valoracion_id' => '3',
                    'descripcion_valoracion' => 'Fusce feugiat mauris non sem tempor tempus. Ut odio enim, sagittis nec sem vel, pulvinar ultrices ligula.',
                ),
                array(
                    'titulo' => 'En la cabeza de Gollum',
                    'editorial' => 'Aragon y la comunidad Editoriales',
                    'fecha_publicacion' => '1995-12-01',
                    'precio_venta' => '16000',
                    'precio_minorista' => '14000',
                    'valoracion_id' => '2',
                    'descripcion_valoracion' => 'Suspendisse potenti. Aliquam id lorem lacus. Aenean egestas molestie erat, sed dapibus ligula.',
                ),
                array(
                    'titulo' => '¿Quien tomo la calavera de cristal?',
                    'editorial' => 'Indi',
                    'fecha_publicacion' => '1964-01-09',
                    'precio_venta' => '12000',
                    'precio_minorista' => '10000',
                    'valoracion_id' => '3',
                    'descripcion_valoracion' => 'Pellentesque a ligula tristique, porttitor enim eget, rutrum augue. Fusce accumsan rutrum sollicitudin. Sed tempus a sapien id rhoncus. Donec vestibulum elit ut sem luctus dictum.',
                ),
                array(
                    'titulo' => 'La nueva era',
                    'editorial' => 'Editoriales Yoda',
                    'fecha_publicacion' => '1999-12-12',
                    'precio_venta' => '26000',
                    'precio_minorista' => '22000',
                    'valoracion_id' => '1',
                    'descripcion_valoracion' => 'Pellentesque a ligula tristique, porttitor enim eget, rutrum augue. Fusce accumsan rutrum sollicitudin. Sed tempus a sapien id rhoncus. Donec vestibulum elit ut sem luctus dictum.',
                ),
                array(
                    'titulo' => 'Ensayo sobre la ceguera 2',
                    'editorial' => 'Editoriales ABC',
                    'fecha_publicacion' => '1999-12-12',
                    'precio_venta' => '22000',
                    'precio_minorista' => '17000',
                    'valoracion_id' => '3',
                    'descripcion_valoracion' => 'Pellentesque a ligula tristique, porttitor enim eget, rutrum augue. Fusce accumsan rutrum sollicitudin. Sed tempus a sapien id rhoncus. Donec vestibulum elit ut sem luctus dictum.',
                ),
            );

            foreach ($lista_libros as $data) {
                Libro::create($data);
            }
        }
    }
}
