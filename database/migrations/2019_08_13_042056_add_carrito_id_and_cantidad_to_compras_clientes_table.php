<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCarritoIdAndCantidadToComprasClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compras_clientes', function (Blueprint $table) {
            //
            $table->string('carrito_id')->after('empleado_id')->nullable();
            $table->integer('cantidad')->after('carrito_id')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compras_clientes', function (Blueprint $table) {
            //
            $table->dropColumn('carrito_id');
            $table->dropColumn('cantidad');


        });
    }
}
