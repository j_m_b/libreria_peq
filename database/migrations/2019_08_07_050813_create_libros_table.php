<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('titulo');
            $table->text('editorial');
            $table->date('fecha_publicacion');
            $table->integer('precio_venta');
            $table->integer('precio_minorista');
            $table->unsignedBigInteger('valoracion_id');
            $table->foreign('valoracion_id')->references('id')->on('valoraciones');
            $table->text('descripcion_valoracion')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Scheme::table('libros', function (Blueprint $table){
            $table->dropForeign('libros_valoracion_id_foreign');
        });
        Schema::dropIfExists('libros');
    }
}
