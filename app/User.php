<?php

namespace App;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\CompraCliente;
use App\Models\RegistroCompra;
use App\Models\Libro;
use App\Models\Cliente;
use App\Models\Blog;
use App\Models\FAQ;
use App\User;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;



class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function venta(){
        return $this->hasMany(CompraCliente::class, 'empleado_id');
    }

    public function ventas(){
        return $this->hasMany(RegistroCompra::class, 'empleado_id');
    }

    public function blogs(){
        return $this->hasMany(Blog::class, 'empleado_id');
    }

    public function faqs(){
        return $this->hasMany(FAQ::class, 'empleado_id');
    }
}
