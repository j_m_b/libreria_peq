<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;
use Illuminate\Support\Facades\Hash;

use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Auth\RegistersUsers;


class CreateAdminData extends Command
{
    use RegistersUsers, HasRoles;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:initial-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register initial data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        //Check de que los registros no existan
        // $super_admin_user = User::where('name', 'admin')->first();
        // $roles = Role::get();

        // if(!$super_admin_user && !$roles){
        //     //Creo los roles
        //     $role1 = Role::create([
        //         'name' => 'super-admin',
        //         'guard_name' => 'web'
        //         ]);
    
        //     $role2 = Role::create([
        //         'name' => 'admin',
        //         'guard_name' => 'web'
        //         ]);
    
        //     $role3 = Role::create([
        //         'name' => 'empleado',
        //         'guard_name' => 'web'
        //         ]);
    
        //     $role4 = Role::create([
        //         'name' => 'cliente',
        //         'guard_name' => 'web'
        //         ]);
    
        //     //Creado a admin
        //     $user = User::create([
        //         'name' => 'admin',
        //         'email' => 'admin@admin.com',
        //         'password' => Hash::make('essau.co'),
        //         ]);
    
        //     $user->assignRole('super-admin');

        // }

        $this->info('done');
    }
}
