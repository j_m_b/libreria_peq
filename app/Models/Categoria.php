<?php

namespace App\Models;
use App\Models\Blog;
use Illuminate\Database\Eloquent\SoftDeletes;


use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //
    use SoftDeletes;

    public $table = 'categorias';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
        'categoria'
    ];

    public function blogs()
    {
        return $this->belongsToMany(Blog::class, 'categorias_blogs', 'categoria_id', 'blog_id')->withTimestamps();;
    }
}
