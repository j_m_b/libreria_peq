<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contactenos extends Model
{
    //
    use SoftDeletes;

    public $table = 'contactenos';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
        'nombres',
        'apellidos',
        'telefono',
        'email',
        'pregunta',
        'respuesta',
        'estado',
        'empleado_id'
    ];

    public function empleado(){
        return $this->belongsTo(User::class, 'empleado_id');
    }
}
