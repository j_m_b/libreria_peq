<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Valoracion;
use App\Models\Autor;
use App\Models\Cliente;
use Laravel\Scout\Searchable;





class Libro extends Model
{
    //
    use SoftDeletes;
    use Searchable;


    public $table = 'libros';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
        'titulo',
        'imagen',
        'editorial',
        'fecha_publicacion',
        'precio_venta',
        'precio_minorista',
        'valoracion_id',
        'descripcion_valoracion'
    ];

    public function autores()
    {
        return $this->belongsToMany(Autor::class, 'libros_autores', 'libro_id', 'autor_id')->withTimestamps();
    }

    public function ventas()
    {
        return $this->belongsToMany(Cliente::class, 'compras_clientes', 'libro_id', 'cliente_id')->withTimestamps();
    }

    public function valoracion(){
        return $this->belongsTo(Valoracion::class, 'valoracion_id');
    }

    public function searchableAs()
    {
        return 'posts_index';
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();
        // Customize array...
        return $array;
    }
}
