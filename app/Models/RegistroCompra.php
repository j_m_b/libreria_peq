<?php

namespace App\Models;
use App\Models\CompraCliente;
use App\Models\Cliente;
use App\User;




use Illuminate\Database\Eloquent\Model;

class RegistroCompra extends Model
{
    //
    public $table = 'registro_compra';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
        'cliente_id',
        'registro',
        'total',
        'empleado_id',
    ];

    public function compras(){
        return $this->hasMany(CompraCliente::class, 'carrito_id');
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class, 'cliente_id');
    }

    public function empleado()
    {
        return $this->belongsTo(User::class, 'empleado_id');
    }

}
