<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Models\Categoria;




class Blog extends Model
{
    //
    use SoftDeletes;

    public $table = 'blog';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
        'empleado_id',
        'titulo',
        'slug',
        'imagen',
        'contenido',
        'estado'
    ];

    public function empleado(){
        return $this->belongsTo(User::class, 'empleado_id');
    }

    public function categorias()
    {
        return $this->belongsToMany(Categoria::class, 'categorias_blogs', 'blog_id', 'categoria_id')->withTimestamps();;
    }
}
