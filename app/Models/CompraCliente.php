<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Cliente;
use App\Models\Libro;
use App\Models\RegistroCompra;



class CompraCliente extends Model
{
    //
    public $table = 'compras_clientes';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
        'cliente_id',
        'libro_id',
        'empleado_id',
        'carrito_id',
        'cantidad'
    ];

    public function cliente(){
        return $this->belongsTo(Cliente::class, 'cliente_id');
    }

    public function libro(){
        return $this->belongsTo(Libro::class, 'libro_id');
    }

    public function empleado(){
        return $this->belongsTo(User::class, 'empleado_id');
    }

    public function registro(){
        return $this->belongsTo(RegistroCompra::class, 'carrito_id');
    }
}
