<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Autor;
use App\Models\Cliente;


class RelacionLibroAutor extends Model
{
    //
    use SoftDeletes;

    public $table = 'libros_autores';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
        'libro_id',
        'autor_id'
    ];

    public function libro(){
        return $this->belongsTo(Libro::class, 'libro_id');
    }

    public function autor(){
        return $this->belongsTo(Autor::class, 'autor_id');
    }
}
