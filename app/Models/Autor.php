<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Libro;
use Laravel\Scout\Searchable;




class Autor extends Model
{
    //
    use SoftDeletes;
    use Searchable;


    public $table = 'autores';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
        'nombre',
        'apellido',
        'fecha_nacimiento',
        'fecha_muerte'
    ];

    public function libros()
    {
        return $this->belongsToMany(Libro::class, 'libros_autores', 'autor_id', 'libro_id')->withTimestamps();;
    }

    public function searchableAs()
    {
        return 'posts_index';
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();
        // Customize array...
        return $array;
    }



    
}
