<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Libro;


class Valoracion extends Model
{
    //
    use SoftDeletes;

    public $table = 'valoraciones';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
      'valoracion'
    ];

    public function libro(){
      return $this->hasOne(Libro::class, 'valoracion_id');
    }

}
