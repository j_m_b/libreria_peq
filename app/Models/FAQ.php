<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;



class FAQ extends Model
{
    //
    use SoftDeletes;

    public $table = 'faq';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
        'empleado_id',
        'pregunta',
        'respuesta',
        'orden',
    ];

    public function empleado(){
        return $this->belongsTo(User::class, 'empleado_id');
    }

}
