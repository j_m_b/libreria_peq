<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\CompraCliente;
use App\Models\RegistroCompra;
use App\Models\Libro;



class Cliente extends Model
{
    //
    use SoftDeletes;

    public $table = 'clientes';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
        'nombre',
        'apellido',
        'num_telefono',
        'direccion_envio',
        'email'
    ];

    public function compras()
    {
        return $this->belongsToMany(Libro::class, 'compras_clientes', 'cliente_id', 'libro_id')->withTimestamps();;
    }

    public function registros(){
        return $this->hasMany(RegistroCompra::class, 'cliente_id');
    }

}
