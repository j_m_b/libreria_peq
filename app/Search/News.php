<?php

namespace App\Search;

use Algolia\ScoutExtended\Searchable\Aggregator;

class News extends Aggregator
{
    /**
     * The names of the models that should be aggregated.
     *
     * @var string[]
     */
    protected $models = [
        // ..
        \App\Models\Libro::class,
        \App\Models\Autor::class,        
    ];

    // protected $relations = [
    //     \App\Models\Libro::class => ['autores'],
    //     \App\Models\Autor::class => ['libros'],
    // ];
}
