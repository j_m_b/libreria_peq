<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DataTables;
use App\User;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Auth;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Hash;






class CuentaController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:super-admin|admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $cuentas = User::get();
            return view('cuentas.index', compact('cuentas'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        try {
            return view('auth.register');            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $agregar_cuenta = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
                ]);
            $agregar_cuenta->assignRole('empleado');

            Alert::success('Cuenta agregada exitosamente');  
            return redirect()->route('cuentas.index');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $cuenta = User::find($id);
            $roles_activos = $cuenta->getRoleNames()->toArray();
            $roles = Role::get();
            return view('cuentas.show', compact('cuenta', 'roles', 'roles_activos'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            //dd(\array_diff($request->roles, ["1"]));
            $cuenta = User::find($id);

            $actualizar_cuenta = User::where('id', $id)->update([
                'name' => $request->name,
                'email' => $request->email
            ]);

            if (!Auth::user()->hasRole('super-admin')) {
                $actualizar_roles = \array_diff($request->roles, ["1"]);
                $cuenta->syncRoles($actualizar_roles);
            }else {
                $actualizar_roles = $request->roles;
                $cuenta->syncRoles($actualizar_roles);
            }

            Alert::success('Cuenta actualizada exitosamente');  
            return redirect()->route('cuentas.index');
            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function update_password(Request $request, $id)
    {
        //
        //dd($request);
        try {
            if ($request->password == $request->password_comprobacion) {
                $cuenta = User::find($id);
                if ($cuenta->id == Auth::user()->id) {
                    $cuenta->password = Hash::make($request->password);
                    $cuenta->save();
                    Alert::warning('Debera reingresar al sistema', 'Cambio realizado exitosamente');                
                    return redirect()->route('logout');
                } else {
                    $cuenta->password = Hash::make($request->password);
                    $cuenta->save();
                    Alert::success('Cuenta actualizada exitosamente');  
                    return redirect()->route('cuentas.index');
                }
            } else {
                Alert::warning('Las contraseñas no coinciden');                
                return redirect()->route('cuentas.index');
            }
            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
