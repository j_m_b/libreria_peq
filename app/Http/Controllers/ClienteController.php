<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\CompraCliente;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DataTables;



class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $clientes = Cliente::with('compras')->get();
            return view('clientes.index', compact('clientes'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function table()
    {
        //
        try {
            $clientes_lista = Cliente::with('compras')->get();
            return DataTables::collection($clientes_lista)->make(true);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('clientes.agregar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            if (!$this->cliente_existente($request)) {
                $guardar_cliente = Cliente::create([
                    'nombre' => $request->nombre,
                    'apellido' => $request->apellido,
                    'num_telefono' => $request->num_telefono,
                    'direccion_envio' => $request->direccion_envio,
                    'email' => $request->email
                ]);
                Alert::success('Cliente agregado exitosamente');  
                return redirect()->route('clientes.show', ['id'=>$guardar_cliente->id]);
            } else {
                Alert::error('Cliente existente');
                return back();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $cliente = Cliente::with(['registros' => function($query){
                $query->with(['compras' => function($query){
                    $query->with(['libro' => function($query){
                        $query->with('autores');
                    }]);
                }]);
            }])->find($id);
            //$cliente = Cliente::with('registros')->find($id);
            return view('clientes.actualizar', compact('cliente'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cliente $cliente, $id)
    {
        //
        try {
            if (!$this->cliente_existente($request)) {
                $actualizar_cliente = Cliente::where('id', $id)->update([
                    'nombre' => $request->nombre,
                    'apellido' => $request->apellido,
                    'num_telefono' => $request->num_telefono,
                    'direccion_envio' => $request->direccion_envio,
                    'email' => $request->email
                ]);
                $cliente = Cliente::find($id);                
                Alert::success('Cliente actualizado exitosamente');  
                return redirect()->route('clientes.show', ['id'=>$cliente->id]);
            } else {
                Alert::error('Cliente existente con mismos datos');
                return back();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */

    public function cliente_existente($request){
        //dd($request);

        $cliente_existente = Cliente::where('nombre', $request->nombre)
                ->where('apellido', $request->apellido)
                ->where('num_telefono', $request->num_telefono)
                ->where('direccion_envio', $request->direccion_envio)
                ->where('email', $request->email)
                ->first();
        if ($cliente_existente) {
            return true;
        } else {
            return false;
        }
    }

    public function realizar_venta(Request $request){
        try {
            //id_carrito = ($fecha+$cliente_id+$user_id+random(100))
            $fecha = date_format($date, 'd-m-y');
            $cliente_id = $request->cliente_id;
            $user_id = Auth::user()->id;
            //verifico la existencia del id_carrito en bases de datos
            do {
                $carrito_id = null;
                $carrito_id = $fecha+$cliente_id+$user_id+rand(1+100);
                $existencia_carrito_id = CompraCliente::where('carrito_id', $carrito_id)->exists();
            } while ($existencia_carrito_id == true);


            $cliente = Cliente::find($request->cliente_id);
            foreach ($request->libro_id as $libro_id) {
                $cliente->ventas()->attach([
                    'libro_id' => $libro_id, 
                    'empleado_id' => Auth::user()->id,
                    'carrito_id' => $carrito_id,
                    'cantidad' => $request->cantidad
                ]);
            }
            Alert::success('Venta creada');  
            return redirect()->route('ventas.resumen-venta', ['id'=>$guardar_cliente->id]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function destroy(Cliente $cliente, $id)
    {
        //
        try {
            $borrar_cliente = Cliente::find($id)->delete();
            return redirect()->route('clientes.index');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
