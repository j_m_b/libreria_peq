<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;



use App\User;
use DataTables;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\CompraCliente;
use App\Models\Cliente;
use App\Models\Libro;
use App\Models\Valoracion;
use App\Models\RegistroCompra;






use Illuminate\Http\Request;

class CompraClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $ventas = CompraCliente::with('cliente')->with('libro')->with('empleado')->get();
            return view('compras.index', compact('ventas'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //
        try {
            $cliente = Cliente::find($id);
            return view('compras.agregar', compact('cliente'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function show($id)
    {
        //
        try {
            $libros_compra = CompraCliente::with(['libro' => function($query){
                $query->with('autores');}])
                ->with('empleado')
                ->with('cliente')
                ->where('carrito_id', $id)
                ->get();
            return response()->json($libros_compra);            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function table_usuario($id)
    {
        //
        try {
            $cliente = RegistroCompra::with(['compras' => function($query){
                $query->with(['libro' => function($query){
                    $query->with('autores'); 
                }]);
            }])->where('cliente_id',$id)->get();
            //dd($cliente);
            return DataTables::collection($cliente)->make(true);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function table()
    {
        //
        try {
            $ventas = RegistroCompra::with(['compras' => function($query){
                $query->with(['libro' => function($query){
                        $query->with('autores');
                }]);
                }])->with('cliente')->with('empleado')->get();
            return DataTables::collection($ventas)->make(true);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        try {
            //dd(Auth::user()->id);
            //carrito_id = ($fecha+$cliente_id+$user_id+random(100))
            //carrito_id es el id del registro de compra
            $fecha = date('Y-m-d');
            $cliente_id = $request->cliente_id;
            $user_id = Auth::user()->id;
            //verifico la existencia del id_carrito en bases de datos
            do {
                $carrito_id = null;
                $carrito_id = $fecha.'+'.$cliente_id.'+'.$user_id.'+'.rand(1,100);
                $existencia_carrito_id = RegistroCompra::where('registro', $carrito_id)->exists();
            } while ($existencia_carrito_id == true);

            $crear_registro_compra = RegistroCompra::create([
                'cliente_id' => $cliente_id,
                'registro' => $carrito_id,
                'empleado_id' => $user_id
            ]);
            $id_registro_compra = $crear_registro_compra->id;


            $cliente = Cliente::find($cliente_id);
            $index = 0;
            $total_compra = 0;
            foreach ($request->libro_id as $libro_id) {
                //dd($user_id);
                $cliente->compras()->attach($libro_id, [
                    'empleado_id' => $user_id,
                    'carrito_id' => $id_registro_compra,
                    'cantidad' => $request->cantidad_libro[$index]
                ]);
                $libro = Libro::where('id', $libro_id)->first();
                $costo_libro_cantidad = $libro->precio_venta * $request->cantidad_libro[$index];
                $total_compra = $total_compra + $costo_libro_cantidad;
                $index++;
            }

            $actualizar_registro_compra = RegistroCompra::where('id', $id_registro_compra)->update([
                'total' => $total_compra
            ]);

            Alert::success('Venta creada');  
            return redirect()->route('clientes.show', ['id'=>$cliente_id]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CompraCliente  $compraCliente
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CompraCliente  $compraCliente
     * @return \Illuminate\Http\Response
     */
    public function edit(CompraCliente $compraCliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CompraCliente  $compraCliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompraCliente $compraCliente)
    {
        //
        try {
            //code...
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CompraCliente  $compraCliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompraCliente $compraCliente, $id)
    {
        //
        try {
            $borrar_compra = CompraCliente::find($id)->delete();
            return back();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
