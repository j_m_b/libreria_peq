<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Models\Autor;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;


use DataTables;

class AutorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $autores = Autor::with('libros')->get();
            return view('autores.index', compact('autores'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function table()
    {
        //
        try {
            $autores_lista = Autor::with('libros')->get();
            return DataTables::collection($autores_lista)->make(true);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('autores.agregar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request);
        try {
            if (!$this->autor_existente($request)) {
                $guardar_autor = Autor::create([
                    'nombre' => $request->nombre,
                    'apellido' => $request->apellido,
                    'fecha_nacimiento' => $request->fecha_nacimiento,
                    'fecha_muerte' => $request->fecha_muerte
                ]);
                Alert::success('Autor agregado exitosamente');  
                return redirect()->route('autores.show', ['id'=>$guardar_autor->id]);
            } else {
                Alert::error('Autor existente');
                return back();
            }
            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Autor  $autor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $autor = Autor::with('libros')->find($id);
            return view('autores.actualizar', compact('autor'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Autor  $autor
     * @return \Illuminate\Http\Response
     */
    public function edit(Autor $autor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Autor  $autor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //dd($id, $request);
        try {
            if (!$this->autor_existente($request)) {
                $actualizar_autor = Autor::where('id', $id)->update([
                    'nombre' => $request->nombre,
                    'apellido' => $request->apellido,
                    'fecha_nacimiento' => $request->fecha_nacimiento,
                    'fecha_muerte' => $request->fecha_muerte
                ]);
                $autor = Autor::find($id);
                $autor->libros()->sync($request->libros);
                
                Alert::success('Autor actualizado exitosamente');  
                return redirect()->route('autores.show', ['id'=>$autor->id]);
            } else {
                Alert::error('Autor existente con mismos datos');
                return back();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Autor  $autor
     * @return \Illuminate\Http\Response
     */
    public function autor_existente($request){
        //dd($request);
        try {
            $autor_existente = Autor::where('nombre', $request->nombre)
                    ->where('apellido', $request->apellido)
                    ->where('fecha_nacimiento', $request->fecha_nacimiento)
                    ->where('fecha_muerte', $request->fecha_muerte)
                    ->first();
            if ($autor_existente) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function agregar_libro($id, Request $request){
        try {
            $autor = Autor::find($id);
            $registro_existente = $autor->libros->contains($request->libro_id);
            if (!$registro_existente) {
                $autor->libros()->attach($request->libro_id);
                return response()->json(['response' => 'ok']); 
            } else {
                return response()->json(['response' => 'registro existente']); 
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function eliminar_libro($id, Request $request){
        try {
            $autor = Autor::find($id);
            $registro_existente = $autor->libros->contains($request->libro_id);
            if ($registro_existente) {
                $autor->libros()->detach($request->libro_id);
                return response()->json(['response' => 'ok']); 
            } else {
                return response()->json(['response' => 'imposible eliminar registro, no existe']); 
            }  
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /////////
    ///////// el retorno de esta funcion se puede generar en la funcion show,
    /////////
    public function refrescar_libros(Request $request, $id){
        try {
            $autor = Autor::where('id',$id)->with('libros')->first();
            return response()->json($autor);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function destroy(Autor $autor, $id)
    {
        //
        try {
            $borrar_autor = Autor::find($id)->delete();
            return redirect()->route('autores.index');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
