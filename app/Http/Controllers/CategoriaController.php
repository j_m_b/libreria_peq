<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Categoria;
use RealRashid\SweetAlert\Facades\Alert;




class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            //code...
            $categorias = Categoria::get();
            return view('blog.categorias.index', compact('categorias'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            if (!$this->categoria_existente($request)) {
                $crear_categorias = Categoria::create([
                    'categoria' => $request->categoria
                ]);
                Alert::success('Categoria creada exitosamente');  
                return redirect()->route('blog.categorias.index');                
            } else {
                Alert::error('Categoria existente con mismos datos');
                return back();
            }
            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            //code...
            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function add(Request $request, $id)
    {
        //
        try {
            $blog = Blog::find($id);
            $actualizar_categorias = $request->categorias;
            $blog->categorias()->sync($actualizar_categorias);
            Alert::success('Categorias modificadas exitosamente');  
            return redirect()->route('blog.show', ['slug'=>$blog->slug]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            if (!$this->categoria_existente($request)) {
                $editar_categorias = Categoria::where('id', $id)->update([
                    'categoria' => $request->categoria
                ]);
                Alert::success('Categoria modificada exitosamente');  
                return redirect()->route('blog.categorias.index');            
            } else {
                Alert::error('Categoria existente con mismos datos');
                return back();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function categoria_existente($request){
        try {
            $categoria_existente = Categoria::where('categoria', $request->categoria)->first();
            if ($categoria_existente) {
                return true;
            } else {
                return false;
            }
            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $borrar_autor = Categoria::find($id)->delete();
            Alert::success('Categoria eliminada exitosamente');  
            return redirect()->route('blog.categorias.index');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
