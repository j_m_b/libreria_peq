<?php

namespace App\Http\Controllers;

use App\Models\Libro;
use App\Models\Valoracion;
use App\Models\Autor;
use Illuminate\Http\Request;
use DataTables;
use Response;
use RealRashid\SweetAlert\Facades\Alert;
use Auth;
use Illuminate\Support\Facades\Storage;


//use Alert;


class LibroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $libros = Libro::with('autores')->with('valoracion')->get();
            return view('libros.index', compact('libros'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function table()
    {
        //
        try {
            $libros_lista = Libro::with('autores')->with('valoracion')->get();
            return DataTables::collection($libros_lista)->make(true);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $valoracion = Valoracion::all();
        return view('libros.agregar', compact('valoracion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request);
        try {
            if (!$this->libro_existente($request)) {
                $imagen = $this->store_imagen_libro($request);
                $guardar_libro = Libro::create([
                    'titulo' => $request->titulo,
                    'imagen' => $imagen,
                    'editorial' => $request->editorial,
                    'fecha_publicacion' => $request->fecha_publicacion,
                    'precio_venta' => $request->precio_venta,
                    'precio_minorista' => $request->precio_minorista,
                    'valoracion_id' => $request->valoracion_id,
                    'descripcion_valoracion' => $request->descripcion_valoracion
                ]);
                Alert::success('Libro agregado exitosamente');  
                return redirect()->route('libros.show', ['id'=>$guardar_libro->id]);
            } else {
                Alert::error('Libro existente');
                return back();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //dd($id);
        try {
            $libro = Libro::where('id',$id)->with('autores')->first();
            $valoracion = Valoracion::all();
            return view('libros.actualizar', compact('libro', 'valoracion'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function edit(Libro $libro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Libro $libro, $id)
    {
        //
        //dd($id, $request);
        try {
            if (!$this->libro_existente($request)) {
                if (!$request->hasFile('imagen')) {
                    $imagen = $request->portada_actual;
                }else {
                    $imagen = $this->store_imagen_libro($request);
                }

                $actualizar_libro = Libro::where('id', $id)->update([
                    'titulo' => $request->titulo,
                    'imagen' => $imagen,
                    'editorial' => $request->editorial,
                    'fecha_publicacion' => $request->fecha_publicacion,
                    'precio_venta' => $request->precio_venta,
                    'precio_minorista' => $request->precio_minorista,
                    'valoracion_id' => $request->valoracion_id,
                    'descripcion_valoracion' => $request->descripcion_valoracion
                ]);
                //actualizo autores de libro
                $libro = Libro::find($id);
                $libro->autores()->sync($request->autores);

                Alert::success('Libro actualizado exitosamente');  
                return redirect()->route('libros.show', ['id'=>$libro->id]);
            } else {
                Alert::error('Libro existente con mismos datos');
                return back();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Libro  $libro
     * @return \Illuminate\Http\Response
     */

    public function libro_existente($request){
        try {
            $libro_existente = Libro::where('titulo', $request->titulo)
                    ->where('editorial', $request->editorial)
                    ->where('fecha_publicacion', $request->fecha_publicacion)
                    ->where('precio_venta', $request->precio_venta)
                    ->where('precio_minorista', $request->precio_minorista)
                    ->where('valoracion_id', $request->valoracion_id)
                    ->first();
            if ($libro_existente) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function agregar_autor($id, Request $request){

        try {
            //dd($libro->autores->contains(2));
            $libro = Libro::find($id);
            $registro_existente = $libro->autores->contains($request->autor_id);
            if (!$registro_existente) {
                $libro->autores()->attach($request->autor_id);
                return response()->json(['response' => 'ok']); 
            } else {
                return response()->json(['response' => 'registro existente']); 
            }  
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function eliminar_autor($id, Request $request){
        try {
            $libro = Libro::find($id);
            $registro_existente = $libro->autores->contains($request->autor_id);
            if ($registro_existente) {
                $libro->autores()->detach($request->autor_id);
                return response()->json(['response' => 'ok']); 
            } else {
                return response()->json(['response' => 'imposible eliminar registro, no existe']); 
            }  
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /////////
    ///////// el retorno de esta funcion se puede generar en la funcion show,
    /////////
    public function refrescar_autores(Request $request, $id){
        try {
            $libro = Libro::where('id',$id)->with('autores')->first();
            return response()->json($libro);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function destroy($id)
    {
        //
        //dd($id);
        try {
            $borrar_libro = Libro::find($id);
            $eliminar_portada = Storage::delete($borrar_libro->imagen);
            $borrar_libro->delete();

            return redirect()->route('libros.index');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
