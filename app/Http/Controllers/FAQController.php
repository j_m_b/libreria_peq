<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\User;
use App\Models\FAQ;
use RealRashid\SweetAlert\Facades\Alert;



class FAQController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $faqs = FAQ::with('empleado')->get();
            return view('faq.index', compact('faqs'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            if (!$this->faq_existente($request)) {
                $crear_FAQ = FAQ::create([
                    'empleado_id' => Auth::user()->id,
                    'pregunta' => $request->pregunta,
                    'respuesta' => $request->respuesta
                ]);
                Alert::success('FAQ creada exitosamente');  
                return redirect()->route('faq.index');
            } else {
                Alert::error('FAQ existente con mismos datos');
                return back();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $faq = FAQ::with('empleado')->find($id);
            return view('faq.show', compact('faq'));            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            if (!$this->faq_existente($request)) {
                $editar_faq = FAQ::where('id',$id)->update([
                    'pregunta' => $request->pregunta,
                    'respuesta' => $request->respuesta
                ]);
                Alert::success('FAQ modificada exitosamente');  
                return redirect()->route('faq.index');
            } else {
                Alert::error('FAQ existente con mismos datos');
                return back();
            }
            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function faq_existente($request){
        //
        try {
            $faq_existente = FAQ::where('pregunta', $request->pregunta)->first();
            if ($faq_existente) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $eliminar_faq = FAQ::find($id);
            $eliminar_faq->delete();
            Alert::success('FAQ eliminada exitosamente');  
            return redirect()->route('faq.index');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
