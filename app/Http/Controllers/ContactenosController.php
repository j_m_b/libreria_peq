<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Contactenos;
use RealRashid\SweetAlert\Facades\Alert;
use Auth;
use DataTables;




class ContactenosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $mensajes = Contactenos::where('estado', false)->with('empleado')->get();
            return view('home', compact('mensajes'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function all()
    {
        //
        try {
            $mensajes = Contactenos::with('empleado')->get();
            return view('contactenos.index', compact('mensajes'));
            return DataTables::collection($mensajes)->make(true);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function all_table()
    {
        //
        try {
            $mensajes = Contactenos::with('empleado')->orderBy('estado')->get();
            return DataTables::collection($mensajes)->make(true);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function cambiar_estado()
    {
        //
        try {
            $mensajes = Contactenos::with('empleado')->get();
            return DataTables::collection($mensajes)->make(true);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ESTE METODO SE REPLICA EN ContenidoController, ya que @guest no puede acceder a este controllador
        // try {
        //     //CREAR LOGICA PARA EVITAR LLENAR DB CON INFO TROLL
        //     $crear_mensaje = Contactenos::create([
        //         'nombres' => $request->nombres,
        //         'apellidos' => $request->apellidos,
        //         'telefono' => $request->telefono,
        //         'email' => $request->email,
        //         'pregunta' => $request->pregunta,
        //         'estado' => false
        //     ]);

        //     //CREAR LOGICA PARA ENVIAR MENSAJE A CORREO DE NOTIFICACION DE CREACION DE MENSAJE DE CONTACTENOS

        //     Alert::success('Mensaje creado exitosamente');  
        //     return redirect()->route('inicio');
        // } catch (\Exception $e) {
        //     return $e->getMessage();
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $ver_mensaje = Contactenos::with('empleado')->find($id);
            return response()->json($ver_mensaje);            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            //CREAR LOGICA PARA VERIFICAR QUE MENSAJE NO SE HALLA RESPONDIDO
            $responder_mensaje = Contactenos::where('id', $id)->update([
                'respuesta' => $request->respuesta,
                'estado' => true,
                'empleado_id' => Auth::user()->id
            ]);

            //CREAR LOGICA PARA ENVIAR MENSAJE A EMAIL

            Alert::success('Mensaje respondido exitosamente');  
            return redirect()->route('home');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function update_estado(Request $request, $id)
    {
        //
        try {
            if ($this->verifical_rol()) {
                $mensaje = Contactenos::find($id);
                if ($mensaje->estado) {
                    $nuevo_estado = false;
                } else {
                    $nuevo_estado = true;
                }
                $mensaje->estado = $nuevo_estado;
                $mensaje->save();
                Alert::success('Estado modificado exitosamente');  
                return redirect()->route('home');
                
            } else {
                Alert::error('No puedes alterar el estado de este mensaje', 'no posees autorizacion');  
                return redirect()->route('home');
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            if ($this->verifical_rol()) {
                $eliminar_mensaje = Contactenos::find($id);
                $eliminar_mensaje->delete();
                Alert::success('Mensaje eliminado exitosamente');  
                return redirect()->route('home');
            } else {
                Alert::error('No puedes eliminar este mensaje', 'no posees autorizacion');  
                return redirect()->route('home');
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function verifical_rol(){
        try {
            if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('super-admin')) {
                return true;
            } else {
                return false;                
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
