<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Storage;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function store_blog($request)
    {
        //
        try {
            if ($request->hasFile('imagen')) {
                //Buscar logica para eliminar imagen existente
                if ($request->has('portada_actual')) {
                    Storage::delete($request->portada_actual);
                }

                $ruta = $request->file('imagen')->store('blog_imgs/portada');
                return $ruta;
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function store_imagen_libro($request)
    {
        //
        try {
            if ($request->hasFile('imagen')) {
                //Buscar logica para eliminar imagen existente
                if ($request->has('portada_actual')) {
                    Storage::delete($request->portada_actual);
                }

                $ruta = $request->file('imagen')->store('libro_imgs/portada');
                return $ruta;
            }
            else{
                $ruta = 'libro_imgs/portada/default.png';
                return $ruta;
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
