<?php

namespace App\Http\Controllers;

use App\Models\Libro;
use App\Models\Autor;
use App\Models\RelacionLibroAutor;
use Illuminate\Http\Request;

class RelacionLibroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            if ($this->relacion_existente($request)) {
                $crear_relacion = Libro::attach($request->Autor_id);
            } else {
                # code...
            }
            
            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function show(Libro $libro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function edit(Libro $libro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Libro $libro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Libro  $libro
     * @return \Illuminate\Http\Response
     */

    public function relacion_existente($request){
        $relacion_existente = RelacionLibroAutor::where('libro_id', $request->libro_id)
            ->where('autor_id', $request->autor_id)
            ->first();
        if ($relacion_existente) {
            return true;
        } else {
            return false;
        }
        
    }

    public function destroy(Libro $libro)
    {
        //
    }
}
