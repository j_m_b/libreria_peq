<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Libro;
use App\Models\FAQ;
use App\Models\Autor;
use App\Models\Contactenos;
use RealRashid\SweetAlert\Facades\Alert;




class ContenidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_blog()
    {
        //
        try {
            $blogs = blog::with('categorias')->get();
            return view('contenido.blog.index', compact('blogs'));            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function show_faq(){
        try {
            $FAQs = FAQ::get();
            return view('contenido.faq', compact('FAQs'));            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_contactenos(Request $request)
    {
        //
        try {
        //CREAR LOGICA PARA EVITAR LLENAR DB CON INFO TROLL
        $crear_mensaje = Contactenos::create([
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'telefono' => $request->telefono,
            'email' => $request->email,
            'pregunta' => $request->pregunta,
            'estado' => false
        ]);

        //CREAR LOGICA PARA ENVIAR MENSAJE A CORREO DE NOTIFICACION DE CREACION DE MENSAJE DE CONTACTENOS

        Alert::success('Mensaje creado exitosamente','Si escribiste correctamente el email, a este llegara un mensaje de confirmacion de contacto');  
        return redirect()->route('inicio');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_blog($slug)
    {
        //
        try {
            $blog = Blog::with('empleado')->with('categorias')->where('slug', $slug)->first();
            return view('contenido.blog.show', compact('blog'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function search(Request $request)
    {
        try {
            $busqueda = $request->buscar;
            $resultados_libros = Libro::search($busqueda)->paginate(25)->load('autores');
            
            return view('contenido.buscador.resultado', compact('busqueda', 'resultados_libros'));
            //$resultados_autores = Autor::search($request->buscar)->paginate(25)->load('libros');
            // dd($resultados_libros);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function show_libro($id){
        try {
            $libro_detalle = Libro::where('id', $id)->with('autores')->first();
            return response()->json($libro_detalle);            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
