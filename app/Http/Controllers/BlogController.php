<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\FileController;
use App\User;
use Auth;
use App\Models\Blog;
use App\Models\Categoria;
use DataTables;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $blogs = Blog::with('empleado')->get();
            return view('blog.index', compact('blogs'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function table()
    {
        //
        try {
            $blogs = Blog::with('categorias')->with('empleado')->get();
            return DataTables::collection($blogs)->make(true);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        try {
            return view('blog.create');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        try {
            //guardo imagen de portada si existe
            $portada = $this->store_blog($request);
            $contenido = $this->generar_contenido($request);
            $slug = $this->generar_slug($request);
            $crear_blog = Blog::create([
                'titulo' => $request->titulo,
                'empleado_id' => Auth::user()->id,
                'slug' => $slug,
                'imagen' => $portada,
                'contenido' => $contenido,
                'estado' => false
            ]);
            Alert::success('Blog creado exitosamente');  
            return redirect()->route('blog', ['slug'=>$crear_blog->slug]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
        try {
            $blog = Blog::with('empleado')->with('categorias')->where('slug', $slug)->first();
            $lista = $blog->categorias;
            $categorias_activas[] = null;
            foreach ($lista as $categoria_activa) {
                $categorias_activas[] = $categoria_activa->categoria;
            }
            $categorias = Categoria::get();
            return view('blog.show', compact('blog', 'categorias_activas', 'categorias'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            $blog = Blog::find($id);                

            if ($blog->titulo != $request->titulo) {
                $slug = $this->generar_slug($request);
            }else {
                $slug = $blog->slug;
            }

            if (!$request->hasFile('imagen')) {
                $portada = $request->portada_actual;
            }else {
                $portada = $this->store_blog($request);
            }

            $contenido = $this->generar_contenido($request);
            //dd($contenido);

            $editar_blog = Blog::where('id', $id)->update([
                'titulo' => $request->titulo,
                'slug' => $slug,
                'imagen' => $portada,
                'contenido' => $contenido
            ]);
            Alert::success('Blog editado exitosamente');  
            return redirect()->route('blog', ['slug'=>$slug]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function generar_contenido($request){
        try {
            
            //Guardo imagenes si existen
            $contenido=$request->input('contenido');
            $dom = new \DomDocument();
            $dom->loadHtml($contenido, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $dom->getElementsByTagName('img');
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}";
            foreach($images as $k => $img){
                $data = $img->getAttribute('src');
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);
                //dd($data);
                $image_name= "/blog_imgs/" . time().$k.'.png';
                $path = public_path() ."/storage". $image_name;
                file_put_contents($path, $data);
                $img->removeAttribute('src');
                $img->setAttribute('src', $actual_link."/storage".$image_name);
            }
            $contenido = $dom->saveHTML();
            return $contenido;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function cambiar_estado($id){
        try {
            $blog = Blog::find($id);
            if ($blog->estado) {
                $estado = false;
            } else {
                $estado = true;
            }
            $blog->estado = $estado;
            $blog->save();
            
            return response()->json(['response' => 'ok']);             
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function generar_slug(Request $request){
        try {
            //verifico existencia de slug
            $slug = Str::slug($request->titulo.'-'.date('Y-m-d'), '-');
            $cantidad_slug = Blog::where('slug', $slug)->count();
            if ($cantidad_slug > 0) {
                $slug = Str::slug($request->titulo.'-'.$cantidad_slug++.'-'.date('Y-m-d'), '-');
            }
            return $slug;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $borrar_blog = Blog::find($id);
            $eliminar_portada = Storage::delete($borrar_blog->imagen);

            $borrar_blog->delete();
            Alert::success('Blog eliminado exitosamente');  
            return redirect()->route('blog.index');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
