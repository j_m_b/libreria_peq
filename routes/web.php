<?php

use App\Models\Libro;
use App\Models\Blog;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//index
Route::get('/', function () {
    $libros = Libro::with('autores')->orderBy('created_at', 'desc')->take(5)->get();
    $blogs = Blog::with('empleado')->with('categorias')->orderBy('created_at', 'desc')->take(5)->get();
    return view('contenido.index', compact('libros','blogs'));
})->name('inicio');

//productos
Route::prefix('productos')->group(function(){
    //index
    Route::get('/', function () {
        return view('contenido.productos.index');
    })->name('productos.index');
    //libros
    Route::get('libros', function () {
        return view('contenido.productos.show-libros');
    })->name('productos.libros');
    Route::get('table-libros', 'LibroController@table')->name('producto-libros.table');
    //autores
    Route::get('autores', function () {
        return view('contenido.productos.show-autores');
    })->name('productos.autores');
    Route::get('table-autores', 'AutorController@table')->name('producto-autores.table');

});

//blog
Route::prefix('blog')->group(function(){
    //todos los blogs
    Route::get('/', 'ContenidoController@index_blog')->name('blogs');
    //detalle de un solo blog
    Route::get('ver/{slug?}', 'ContenidoController@show_blog')->name('blog');
});

//FAQ
Route::get('FAQ', 'ContenidoController@show_faq')->name('faq');

//acerca de nosotros
Route::get('acerca-de-nosotros', function () {
    return view('contenido.acerca-de');
})->name('acerca-de');


//contactenos
Route::prefix('contactenos')->group(function(){
    Route::get('/', function () {
        return view('contenido.contactenos');
    })->name('contactenos');
    Route::post('crear-mensaje', 'ContenidoController@store_contactenos')->name('contactenos.store');
});

//barra buscar
Route::get('buscar', 'ContenidoController@search')->name('buscar');
Route::get('detalle-seleccion-libro/{id?}', 'ContenidoController@show_libro')->name('libro-detalle.show');




Auth::routes();
Auth::routes(['register' => false]);


Route::middleware(['auth', 'role:super-admin|admin|empleado'])->group(function(){
    //home

    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

    Route::get('/home', 'ContactenosController@index')->name('home');

    Route::group(['middleware' => ['auth']], function() {
        Route::resource('roles','RoleController');
        Route::resource('users','UserController');
        Route::resource('products','ProductController');
    });

    //Libros
    Route::prefix('libros')->group(function(){
        Route::get('/', 'LibroController@index')->name('libros.index');
        Route::get('table-libro', 'LibroController@table')->name('libros.table');
        Route::get('agregar-libro', 'LibroController@create')->name('libros.create');
        Route::post('crear-libro', 'LibroController@store')->name('libros.store');
        Route::get('detalle-libro/{id?}', 'LibroController@show')->name('libros.show');
        Route::put('modificar-libro/{id?}', 'LibroController@update')->name('libros.update');
        Route::get('refrescar-autor-libro/{id?}', 'LibroController@refrescar_autores')->name('libros.refrescar-autores');
        Route::post('nuevo-autor-libro/{id?}', 'LibroController@agregar_autor')->name('libros.nuevo-autor');
        Route::post('eliminar-autor-libro/{id?}', 'LibroController@eliminar_autor')->name('libros.eliminar-autor');
        Route::delete('eliminar-libro/{id?}', 'LibroController@destroy')->name('libros.delete');
    });

    //Autores
    Route::prefix('autores')->group(function(){
        Route::get('/', 'AutorController@index')->name('autores.index');
        Route::get('table-autor', 'AutorController@table')->name('autores.table');
        Route::get('agregar-autor', 'AutorController@create')->name('autores.create');
        Route::post('crear-autor', 'AutorController@store')->name('autores.store');
        Route::get('detalle-autor/{id?}', 'AutorController@show')->name('autores.show');
        Route::put('modificar-autor/{id?}', 'AutorController@update')->name('autores.update');
        Route::get('refrescar-libro-autor/{id?}', 'AutorController@refrescar_libros')->name('autores.refrescar-libros');
        Route::post('nuevo-libro-autor/{id?}', 'AutorController@agregar_libro')->name('autores.nuevo-libro');
        Route::post('eliminar-libro-autor/{id?}', 'AutorController@eliminar_libro')->name('autores.eliminar-libro');
        Route::delete('eliminar-autor/{id?}', 'AutorController@destroy')->name('autores.delete');
    });
    //

    //Clientes
    Route::prefix('clientes')->group(function(){
        Route::get('/', 'ClienteController@index')->name('clientes.index');
        Route::get('table-cliente', 'ClienteController@table')->name('clientes.table');
        Route::get('agregar-cliente', 'ClienteController@create')->name('clientes.create');
        Route::post('crear-cliente', 'ClienteController@store')->name('clientes.store');
        Route::get('detalle-cliente/{id?}', 'ClienteController@show')->name('clientes.show');
        Route::put('modificar-cliente/{id?}', 'ClienteController@update')->name('clientes.update');
        Route::delete('eliminar-cliente/{id?}', 'ClienteController@destroy')->name('clientes.delete');
    });

    //Ventas
    Route::prefix('ventas')->group(function(){
        Route::get('/', 'CompraClienteController@index')->name('ventas.index');
        Route::get('vender-a-cliente/{id?}', 'CompraClienteController@create')->name('ventas.create');
        Route::get('table-ventas', 'CompraClienteController@table')->name('ventas.table');
        Route::get('table-ventas-usuario/{id?}', 'CompraClienteController@table_usuario')->name('ventas.table-usuario');
        Route::post('crear-venta', 'CompraClienteController@store')->name('ventas.store');
        Route::get('detalle-venta/{id?}', 'CompraClienteController@show')->name('ventas.show');
        Route::put('modificar-venta/{id?}', 'CompraClienteController@update')->name('ventas.update');
        Route::delete('eliminar-venta/{id?}', 'CompraClienteController@destroy')->name('ventas.delete');
    });

    //only admins
    Route::middleware(['auth', 'role:super-admin|admin'])->group(function(){
        //cuentas
        Route::prefix('cuentas')->group(function(){
            Route::get('/', 'CuentaController@index')->name('cuentas.index');
            Route::get('table-cuenta', 'CuentaController@table')->name('cuentas.table');
            Route::get('detalle-cuenta/{id?}', 'CuentaController@show')->name('cuentas.show');
            Route::put('modificar-cuenta/{id?}', 'CuentaController@update')->name('cuentas.update');
            Route::put('modificar-cuenta-clave/{id?}', 'CuentaController@update_password')->name('cuentas.update-password');
            Route::delete('eliminar-cuenta/{id?}', 'CuentaController@destroy')->name('cuentas.delete');
            //ESTAS RUTAS DEBEN SER DOCUMENTADA SI ES LA PRIMERA VEZ QUE SE USA EL PROGRAMA
            //TAMBIEN HACER AJUSTES EN EL MIDDLEWARE DE Auth/RegisterController
            //EN resources/register.blade.php DEBE CAMBIARSE EL ACTION DEL FORM A "route('register')"
            Route::get('agregar-cuenta', 'CuentaController@create')->name('cuentas.create');
            Route::post('crear-cuenta', 'CuentaController@store')->name('cuentas.store');
            //Route::post('crear-cuenta', '\App\Http\Controllers\Auth\RegisterController@create')->name('cuentas.store');
            ///////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////
        });

        //Blog
        Route::prefix('admin-blog')->group(function(){

            Route::get('/', 'BlogController@index')->name('blog.index');
            Route::get('table-blog', 'BlogController@table')->name('blog.table');
            Route::get('detalle-blog/{slug?}', 'BlogController@show')->name('blog.show');
            Route::put('modificar-blog/{id?}', 'BlogController@update')->name('blog.update');
            Route::put('modificar-estado-blog/{id?}', 'BlogController@cambiar_estado')->name('blog.update.estado');
            Route::put('modificar-blog-clave/{id?}', 'BlogController@update_password')->name('blog.update-password');
            Route::delete('eliminar-blog/{id?}', 'BlogController@destroy')->name('blog.delete');
            Route::get('agregar-blog', 'BlogController@create')->name('blog.create');
            Route::post('crear-blog', 'BlogController@store')->name('blog.store');

            //categoria
            Route::prefix('categorias')->group(function(){
                Route::get('/', 'CategoriaController@index')->name('blog.categorias.index');
                Route::post('crear-categoria', 'CategoriaController@store')->name('blog.categorias.store');
                Route::post('agregar-categoria/{id?}', 'CategoriaController@add')->name('blog.categorias.add');
                Route::put('editar-categoria/{id?}', 'CategoriaController@update')->name('blog.categorias.update');
                Route::delete('eliminar-categoria/{id?}', 'CategoriaController@destroy')->name('blog.categorias.delete');
            });

        });


        //FAQ
        Route::prefix('admin-faq')->group(function(){

            Route::get('/', 'FAQController@index')->name('faq.index');
            Route::get('detalle-faq/{slug?}', 'FAQController@show')->name('faq.show');
            Route::put('modificar-faq/{id?}', 'FAQController@update')->name('faq.update');
            Route::delete('eliminar-faq/{id?}', 'FAQController@destroy')->name('faq.delete');
            Route::post('crear-faq', 'FAQController@store')->name('faq.store');

        });

        //contactenos
        Route::prefix('admin-contactenos')->group(function(){
            Route::get('revisar-mensaje-todos', 'ContactenosController@all')->name('contactenos.all');
            Route::get('revisar-mensaje-todos-table', 'ContactenosController@all_table')->name('contactenos.all-table');
            Route::get('revisar-mensaje/{id?}', 'ContactenosController@show')->name('contactenos.show');
            Route::put('responder-contactenos/{id?}', 'ContactenosController@update')->name('contactenos.update');
            Route::put('cambiar-estado-contactenos/{id?}', 'ContactenosController@update_estado')->name('contactenos.update_estado');
            Route::delete('eliminar-contactenos/{id?}', 'ContactenosController@destroy')->name('contactenos.delete');
        });

    });
    

});
