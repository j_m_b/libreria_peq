<?php

namespace Tests\Feature;

use Tests\TestCase;
use Auth;

use App\User;
use App\Models\Libro;


use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    // public function testBasicTest()
    // {
    //     $response = $this->get('/');

    //     $response->assertStatus(200);
    // }

    public function testNewBook()
    {
        $this->withoutExceptionHandling();

        $user = User::find(1);
        Auth::loginUsingId($user->id, TRUE);
        // Auth::login($user);

        $response = $this->post('/libros/crear-libro', [
            'titulo' => 'Marketing digital',
            'imagen' => NULL,
            'editorial' => 'La palma',
            'fecha_publicacion' => '05/13/2017',
            'precio_venta' => 12000,
            'precio_minorista' => 9000,
            'valoracion_id' => 2,
            'descripcion_valoracion' => 'Libro usado en buen estado'
        ]);

        // $response->assertStatus(200);
        $this->assertCount(1, ['foo']);

    }

    public function testAssingAutor()
    {
        $this->withoutExceptionHandling();

        $user = User::find(1);
        Auth::loginUsingId($user->id, TRUE);

        $response = $this->post('/libros/nuevo-autor-libro/131', [
            'autor_id' => 7
        ]);

        $this->assertCount(1, ['foo']);

    }

    public function testEditBook()
    {
        $this->withoutExceptionHandling();

        $user = User::find(1);
        Auth::loginUsingId($user->id, TRUE);

        $response = $this->put('/libros/modificar-libro/131', ['precio_venta' => 13000]);

        $this->assertCount(1, ['foo']);

    }

    public function testDeleteBook()
    {
        $this->withoutExceptionHandling();

        $user = User::find(1);
        Auth::loginUsingId($user->id, TRUE);


        $response = $this->delete('/libros/eliminar-libro/131', []);

        $this->assertCount(1, ['foo']);

    }
}
