@extends('layouts.app')

@section('title', 'FAQ')

@section('content')
    <div class="container">
        <div>
            <div>
                <h2 class="h2 text-center">FAQs</h2>
            </div>
            <div>
                <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#crearFAQ"><i class="icon_lg_white">@svg("plus_circle", "icon-lg")</i> Crear FAQ</button>
            </div>
            <div class="accordion" id="accordionExample">
                @foreach ($faqs as $faq)   
                <div class="card">
                    <div class="card-header" id="heading{{$faq->id}}">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">
                            {{$faq->pregunta}}
                            </button>
                        </h2>
                    </div>
                
                    <div id="collapse{{$faq->id}}" class="collapse" aria-labelledby="heading{{$faq->id}}" data-parent="#accordionExample">
                        <div class="card-body">
                            <div>
                                <a href="{{route('faq.show',$faq->id)}}">Editar</a>
                            </div>
                            {{$faq->respuesta}}
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </div>

    @include('modals.faq.crear')

    <script>
        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navFAQ').addClass('active border rounded');
        });
    </script>

@endsection