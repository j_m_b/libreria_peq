@extends('layouts.app')

@section('title', 'FAQ')

@section('content')
    <div class="container">
        <div>
            <div>
                <h4 class="h4">Editar FAQ {{$faq->id}}</h5>
            </div>
            <form action="{{ route('faq.update', $faq->id) }}" method="POST" enctype="multipart/form-data">
                <div>
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group row">
                        <label for="inputPregunta" class="col-sm-2 col-form-label">Pregunta</label>
                        <div class="col-sm-10">
                            <input value="{{$faq->pregunta}}" type="text" class="form-control" id="inputPregunta" name="pregunta" placeholder="¿Cual es la pregunta?">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputRespuesta" class="col-sm-2 col-form-label">Respuesta</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="inputRespuesta" name="respuesta" placeholder="Escribe la respuesta">{{$faq->respuesta}}</textarea>                                                    
                        </div>
                    </div>
                </div>
                <div>
                    <button type="submit" class="btn btn-warning"><i class="">@svg("save_icon", "icon-lg")</i> Editar</button>
                    <button type="button" id="eliminar_faq" data-id="{{$faq->id}}" class="btn btn-danger"><i class="icon_lg_white">@svg("delete_icon", "icon-lg")</i> Eliminar</button>
                </div>
                </form>
        </div>
    </div>

    <script>
      $(document).ready(function() {
            $(".nav li").removeClass("active border rounded");
            $('.navFAQ').addClass('active border rounded');

            $("#eliminar_faq").on('click', function(e) {
                e.preventDefault();
                let id = $(this).attr('data-id');
                let urlEliminarFAQ = "{{route('faq.delete')}}/"+id;
                
                Swal.fire({
                    title: 'Eliminarla?',
                    text: "¡Posiblemente este dato se pierda para siempre!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminalo'
                    }).then((result) => {
                    if (result.value) {
                        Swal.fire(
                        'Eliminada exitosamente!',
                        'Esta faq ya no existe.',
                        'success'
                        );
                        $.ajax({
                            url: urlEliminarFAQ,
                            method: 'delete',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        window.location.href = '{{ route("faq.index")}}'; 
                    }
                });
            });
        });
    </script>

@endsection