@extends('layouts.app')

@section('content')
    <div class="container">
        <div>
            <h1 class="h1 text-center">Agregar venta</h1>
        </div>
        <div>
            <div class="p-3 mb-2 bg-light text-dark">
                <div>
                    <h3 class="h3">Detalles del cliente <a href="{{route('clientes.show', $cliente->id)}}">#{{$cliente->id}}</a></h3>
                </div>
                <div class="form-group row">
                    <div class="col"><strong>Cliente:</strong> {{$cliente->nombre}} {{$cliente->apellido}}</div>
                    <div class="col"><strong>Telefono:</strong> {{$cliente->num_telefono}}</div>
                    <div class="w-100"></div>
                    <div class="col"><strong>Correo:</strong> {{$cliente->email}} {{$cliente->apellido}}</div>
                    <div class="col"><strong>Direccion:</strong> {{$cliente->direccion_envio}}</div>
                </div>
            </div>
            <div class="progress" style="height: 1px;">
                <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <hr class="my-4">
            <div class="">
                <form action="{{ route('ventas.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" id="cliente_id" name="cliente_id" value="{{$cliente->id}}">
                    <div>
                        <h3 class="h3">Lista de productos seleccionados</h3>
                    </div>
                    <div class="input-group mb-3">
                        <button type="submit" class="btn btn-outline-info">Realizar compra</button>
                        <span class="input-group-text">Total: </span>
                        <div class="input-group-prepend">
                            <span class="input-group-text">$</span>
                        </div>
                        <input id="totalCompra" type="number" name="total_compra" class="form-control" readonly>
                    </div>
                    <div class="row">
                        <table id="lista-compra" class="table table-dark">
                            <thead>
                                <tr>
                                    <th style="width: 15%">ID</th>
                                    <th style="width: 25%">Titulo</th>
                                    <th style="width: 25%">Editorial</th>
                                    <th style="width: 15%">Precio ($)</th>
                                    <th style="width: 10%">Cantidad</th>
                                    <th style="width: 10%"></th>
                                </tr>
                            </thead>
                            <tbody id="lista-compraBody">
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
            <div class="progress" style="height: 1px;">
                <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <hr class="my-4">
            <div>
                <h3 class="h3">Libros disponibles</h3>
                @include('compras.libros-table')
            </div>
        </div>
    </div>

    <style>
        td.details-control {
            background: url("{{ asset('storage/resources/details_open.png') }}") no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url("{{ asset('storage/resources/details_close.png') }}") no-repeat center center;
        }
    </style>

    <script>
        var urlLibros = "{{ route('libros.table') }}";

        
        function format ( d ) {
            let autor_data = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
            autor_data += '<tr>';
            autor_data += '<td>autores:</td>';
            autor_data += '</tr>';
            $.each(d.autores, function(index, element){
                autor_data += '<tr>';
                autor_data += '<td>('+d.autores[index].id+') '+d.autores[index].apellido+' '+d.autores[index].nombre+' ['+d.autores[index].fecha_nacimiento+' - '+d.autores[index].fecha_muerte+']</td>';
                autor_data += '</tr>';
            });
            autor_data += '</table>';
            return autor_data;
        };

        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navVentas').addClass('active border rounded');

            var table = $('#libros_lista_disponible-table').DataTable({
                "lengthMenu": [[5,10,30, -1], [5,10,30, "Todos"]],
                "processing": true,
                "serverSide": true,
                "ajax": ( {
                    url: urlLibros,
                    pages: 5 // number of pages to cache
                }),
                "columns": [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ''
                    },                  
                    {data: 'id', name: 'id'},                    
                    {data: 'titulo', name: 'titulo'},
                    {data: 'editorial', name: 'editorial'},
                    {data: 'precio_venta', name: 'precio_venta'},
                    {data: 'id', name: 'id',
                        render: function(data, type, row, meta){
                        return '<button type="button" id="refrescarLista" class="btn btn-info agregar-libro" data-id="'+row.id+'" data-titulo="'+row.titulo+'" data-editorial="'+row.editorial+'" data-precio="'+row.precio_venta+'"> <i class="icon_lg_white">@svg("carrito_icon", "icon-lg")</i> Agregar</button>';
                        }
                    },
                ]
            });


            // Add event listener for opening and closing details

            $('#libros_lista_disponible-table tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row( tr );
        
                if ( row.child.isShown() ) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    row.child( format(row.data()) ).show();
                    tr.addClass('shown');
                }
            });


            $('#libros_lista_disponible-table tbody').on('click', 'button.agregar-libro', function (e) {      
                e.preventDefault();
                let libro_id = $(this).attr('data-id');
                let titulo_libro = $(this).attr('data-titulo');
                let editorial_libro = $(this).attr('data-editorial');
                let precio_libro = $(this).attr('data-precio');

                let libro_data = " ";
                libro_data += '<tr>';
                libro_data += '<td>'+libro_id+'</td>';
                libro_data += '<td>'+titulo_libro+'<input type="hidden" name="libro_id[]" class="form-control" value="'+libro_id+'"></td>';
                libro_data += '<td>'+editorial_libro+'</td>';
                libro_data += '<td class="precioLibro" data-cantidad="1">'+precio_libro+'</td>';
                libro_data += '<td><input class="cantidadLibro" type="number" name="cantidad_libro[]" class="form-control" value="1" min="1"></td>';
                libro_data += '<td><button class="remover_libro btn btn-danger" data-id="'+libro_id+'"><i class="icon_lg_white">@svg("remove_icon", "icon-lg")</i></buttos></td>';
                libro_data += '<tr>';

                $('#lista-compraBody').append(libro_data);


                /*Asi evito que se dupliquen filas en la tabla*/
                var seen = {};
                $('#lista-compraBody tr').each(function() {
                var txt = $(this).text();
                if (seen[txt])
                    $(this).remove();
                else
                    seen[txt] = true;
                });

                totalVenta();
                
                $('.cantidadLibro').change(function(){
                    totalVenta();
                });
                
            });

            $(document).on('click', 'button.remover_libro', function () {
                $(this).closest('tr').remove();
                totalVenta();
                return false;
            });

            function totalVenta() {
                let sumTotal = 0;
                let colCantidad = 0;
                $('#lista-compraBody tr .precioLibro').each(function() {
                    var currentRow = $(this).closest("tr");
                    colCantidad = currentRow.find(".cantidadLibro").val(); // get current row 2nd TD
                    valorProducto = parseInt($(this).text()) * colCantidad;
                    sumTotal = sumTotal + valorProducto;
                });
                $('#totalCompra').val(sumTotal);
            };




        });
    </script>
@endsection