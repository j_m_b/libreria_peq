@extends('layouts.app')

@section('title', 'Ventas')

@section('content')

    <div class="container">
        <div>
            <div>
                <h3 class="h2 text-center">Registro de ventas</h3>
            </div>
            <div>
                <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#lista-clientes"><i class="icon_lg_white"> @svg('plus_circle', 'icon-lg')</i> Nueva venta</button>
            </div>
            <table id="ventas-table" class="table display compact nowrap" style="width:100%">
                <thead class="thead-dark">
                    <tr>
                        <th>Registro</th>
                        <th>Nombre cliente</th>
                        <th>Apellido cliente</th>
                        <th>Vendedor</th>
                        <th>Fecha venta</th>
                        <th>Total</th>
                        <th>Detalle</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>


    @include('modals.ventas.clientes')
    @include('modals.ventas.libros-registro')


    <script>
        var urlClientes = "{{ route('clientes.table') }}";
        var urlNuevaVenta = "{{ route('ventas.create') }}/";
        var urlVentas = "{{ route('ventas.table') }}";

        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navVentas').addClass('active border rounded');

            $('#clientes_lista-table').DataTable({
                "lengthMenu": [[5,10,30, -1], [5,10,30, "Todos"]],
                "processing": true,
                "serverSide": true,
                "ajax": ( {
                    url: urlClientes,
                    pages: 5 // number of pages to cache
                }),
                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'nombre', name: 'nombre'},
                    {data: 'apellido', name: 'apellido'},
                    {data: 'num_telefono', name: 'num_telefono'},
                    {data: 'id', name: 'id',
                        render: function(data, type, row, meta){
                            return '<a class="btn btn-info" href="'+urlNuevaVenta+row.id+'"> <i class="icon_lg_white">@svg("dollar_icon", "icon-lg")</i> Vender </a>'

                        }
                    },
                ]
            });

            $('#ventas-table').DataTable({
                "lengthMenu": [[5,10,30, -1], [5,10,30, "Todos"]],
                "processing": true,
                "serverSide": true,
                "ajax": ( {
                    url: urlVentas,
                    pages: 5 // number of pages to cache
                }),
                "columns": [
                    {data: 'registro', name: 'registro'},
                    {data: 'cliente.nombre', name: 'cliente.nombre'},
                    {data: 'cliente.apellido', name: 'cliente.apellido'},
                    {data: 'empleado.name', name: 'empleado.name'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'total', name: 'total'},
                    {data: 'id', name: 'id',
                        render: function(data, type, row, meta){
                            return '<button type="button" data-id="'+row.id+'" data-registro="'+row.registro+'" data-total="'+row.total+'" data-fecha="'+row.created_at+'" class="btn btn-info info_venta" data-toggle="modal" data-target="#detalle_registro_compra" > <i class="icon_lg_white">@svg("eye", "icon-lg")</i> Ver</button>';
                        }
                    },
                ]
            });

            $('#ventas-table tbody').on('click', 'button.info_venta', function (e){
                e.preventDefault();
                //limpio modal
                $( "#clienteCompra" ).empty();
                $( "#registroCompra" ).empty();
                $( "#fechaCompra" ).empty();
                $( "#vendedorCompra" ).empty();
                $( "#totalCompra" ).empty();
                $( "#librosTablaCompra" ).empty();
                //Variables
                let registro_id = $(this).attr('data-id');
                let registro = $(this).attr('data-registro');
                let total = $(this).attr('data-total');
                let fecha_registro = $(this).attr('data-fecha');
                //reasigno valores
                $( "#registroCompra" ).text(registro);
                $( "#fechaCompra" ).text(fecha_registro);
                $( "#totalCompra" ).text('$'+total);
                let urlDetalleCompra = "{{route('ventas.show')}}/"+registro_id;
                let urlDetalleLibro = "{{route('libros.show')}}/";

                $.getJSON(urlDetalleCompra, function (data) {
                    $( "#vendedorCompra" ).text(data[0].empleado.name);
                    let urlCliente = "{{route('clientes.show')}}/"+data[0].cliente_id;
                    $( "#clienteCompra" ).append('<a href="'+urlCliente+'">'+data[0].cliente.nombre+' '+data[0].cliente.apellido+'</a>');
                    let libros_data = " ";
                    $.each(data, function(index, element){
                        libros_data += '<tr>';
                        libros_data += '<td><a href="'+urlDetalleLibro+data[index].libro.id+'">'+data[index].libro.id+'</a></td>';
                        libros_data += '<td>'+data[index].libro.titulo+'</td>';
                        libros_data += '<td><ul>';
                        $.each(data[index].libro.autores, function(i, e){                        
                            libros_data += "<li>"+data[index].libro.autores[i].nombre+" "+data[index].libro.autores[i].apellido+"</li>";
                        });
                        libros_data += '</ul></td>';
                        libros_data += '<td>$'+data[index].libro.precio_venta+'</td>';
                        libros_data += '<td>'+data[index].cantidad+'</td>';
                        libros_data += '<tr>';
                    });
                    $('#librosTablaCompra').append(libros_data);
                });
            });

        });
    </script>
    
@endsection
