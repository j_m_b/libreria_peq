@extends('layouts.app')

@section('title', 'Libros')

@section('content')
    <div class="container">
         <div>
             <h2 class="h2 text-center">Cuentas de empleados</h2>
         </div>
         <div>
            <a href="{{route('cuentas.create')}}" class="btn btn-dark"><i class="icon_lg_white">@svg("add_person_icon", "icon-lg")</i> Agregar cuenta</a>
         </div>
         <div>
             <table class="table table-dark">
                 <thead>
                     <tr>
                         <th>ID</th>
                         <th>Nombre</th>
                         <th>Correo</th>
                         <th>Roles</th>
                         <th>Detalle</th>
                     </tr>
                 </thead>
                 <tbody>
                     @foreach ($cuentas as $empleado)
                     <tr>
                        <td>{{$empleado->id}}</td>
                        <td>{{$empleado->name}}</td>
                        <td>{{$empleado->email}}</td>
                        <td>
                        @foreach ($empleado->getRoleNames() as $rol)
                            <h6><span class="badge badge-primary">{{$rol}}</span></h6>
                        @endforeach
                        </td>
                        <td><a href="{{route('cuentas.show',$empleado->id)}}" class="btn btn-info"><i class="icon_lg_white">@svg("eye", "icon-lg")</i> Ver</a></td>
                     </tr>
                     @endforeach
                 </tbody>
             </table>
         </div>
    </div>

    <script>
        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navCuentas').addClass('active border rounded');
        });


    
    </script>
@endsection