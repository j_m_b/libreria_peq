@extends('layouts.app')

@section('title', 'Libros')

@section('content')
    <div class="container">
        <form action="{{ route('cuentas.update',$cuenta->id) }}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT">
            
            {{ csrf_field() }}
            <div><h1 class="h1">Detalles de la cuenta #{{$cuenta->id}}</h1></div>
            <div class="form-group row">
                <label for="inputNombre" class="col-sm-2 col-form-label">Nombres</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control inputData" id="inputNombre" name="name" value="{{$cuenta->name}}" placeholder="Nombres">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail" name="email" value="{{$cuenta->email}}" placeholder="Correo electronico">
                </div>
            </div>

            <div class="form-group row">
                <label for="inputRoles" class="col-sm-2 col-form-label">Roles</label>
                <ol>
                    @foreach ($roles as $rol)
                    <li>
                        <div class="form-check">
                            @if (in_array($rol->name, $roles_activos))
                            <input class="form-check-input rol" type="checkbox" name="roles[]" value="{{$rol->id}}" checked>
                            @else
                            <input class="form-check-input rol" type="checkbox" name="roles[]" value="{{$rol->id}}">
                            @endif
                            <label class="form-check-label" for="rol_{{$rol->id}}">{{$rol->name}}</label>
                        </div>
                    </li>
                    @endforeach
                </ol>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                <button id="guardarCambios" type="submit" class="btn btn-primary"><i class="icon_lg_white">@svg("save_icon", "icon-lg")</i> Guardar</button>
                <button id="restablecerContraseña" type="button" class="btn btn-warning" data-toggle="modal" data-target="#cambiar-password"><i class="">@svg("change_pass_icon", "icon-lg")</i> Restablecer contraseña</button>
                <a id="eliminarCliente" class="btn btn-danger" href="#"><i class="icon_lg_white">@svg("delete_icon", "icon-lg")</i> Eliminar</a>
                </div>
            </div>
        </form>
    </div>
    @include('modals.cuentas.password')

    <script>
        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded border rounded");
            $('.navCuentas').addClass('active border rounded border rounded');
        });
    </script>
@endsection