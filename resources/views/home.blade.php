@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4 class="h4 text-center">Hola {{Auth::user()->name}}</h4></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-sm"> 
                            <div>
                                <h4 class="h4 text-left">Mensajes recientes</h4>
                                <a href="{{route('contactenos.all')}}">ver todos</a>
                            </div>
                            <div>
                                @if(!$mensajes->isEmpty())    
                                    <table class="table table-dark table-hover">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nombres</th>
                                            <th scope="col">Correo</th>
                                            <th scope="col">Creado el</th>
                                            <th scope="col">Ver</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($mensajes as $mensaje)
                                                <tr>
                                                <th scope="row">{{$mensaje->id}}</th>
                                                <td>{{$mensaje->nombres}} {{$mensaje->apellidos}}</td>
                                                <td>{{$mensaje->email}}</td>
                                                <td>{{$mensaje->created_at}}</td>
                                                <td><button type="button" data-id="{{$mensaje->id}}" class="ver-mensaje btn btn-primary" data-toggle="modal" data-target="#verMensaje-contactenos"><i class="icon_lg_white">@svg("eye", "icon-lg")</i> Ver</button></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div>
                                        <p>No hay ningun mensaje de "Contactenos"</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm"> 
                            <div>
                                <h4 class="h4">Mis mensajes</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('modals.contactenos.responder')

<script>
    $(document).ready(function() {
        $(".nav li").removeClass("active border rounded");
        $('.navPerfil').addClass('active border rounded');

        $(".ver-mensaje").on('click', function(){
            let id = $(this).attr('data-id');
            let urlRevisarMensaje = "{{route('contactenos.show')}}/"+id;
            let urlResponderMensaje = "{{route('contactenos.update')}}/"+id;

            $(".titulo-mensaje").empty();
            $("#nombres").empty();
            $("#telefono").empty();
            $("#email").empty();
            $("#pregunta").empty();
            $(".cambiar_estado").html(' ');

            $.get(urlRevisarMensaje, function( data ) {
                $(".titulo-mensaje").text('Mensaje #: '+data.id);
                $("#nombres").text( data.nombres+' '+data.apellidos);
                $("#telefono").text( data.telefono);
                $("#email").text( data.email);
                $("#pregunta").text( data.pregunta);
                $('#responder-contactenos-form').attr('action', urlResponderMensaje);
                if (data.estado == 1) {
                    $(".cambiar_estado").html('Marcar como no resuelto');
                } else {
                    $(".cambiar_estado").html('Marcar como resuelto');                    
                }
            });
        });

    });

</script>

@endsection
