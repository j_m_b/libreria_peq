@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('autores.update',$autor->id) }}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT">
            
            {{ csrf_field() }}
            <div><h1 class="h1">Detalles del autor #{{$autor->id}}</h1></div>
            <div class="form-group row">
                <label for="inputNombre" class="col-sm-2 col-form-label">Nombres</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control inputData" id="inputNombre" name="nombre" value="{{$autor->nombre}}" placeholder="Nombres">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputApellido" class="col-sm-2 col-form-label">Apellidos</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control inputData" id="inputApellido" name="apellido" value="{{$autor->apellido}}" placeholder="Apellidos">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputFechaNacimiento" class="col-sm-2 col-form-label">Fecha nacimiento</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control inputData" id="inputFechaNacimiento" name="fecha_nacimiento" value="{{$autor->fecha_nacimiento}}" placeholder="Ingrese Fecha nacimiento">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputFechaMuerte" class="col-sm-2 col-form-label">Fecha muerte</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control inputData" id="inputFechaMuerte" name="fecha_muerte" value="{{$autor->fecha_muerte}}" placeholder="Ingrese Fecha muerte">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                <button id="guardarCambios" type="submit" class="btn btn-primary" disabled><i class="icon_lg_white">@svg("save_icon", "icon-lg")</i> Guardar</button>
                <a id="eliminarAutor" class="btn btn-danger" href="#"><i class="icon_lg_white">@svg("delete_icon", "icon-lg")</i> Eliminar</a>
                </div>
            </div>
        </form>
        <form action="">
            <div>
                <h1 class="h1">Libros </h1>
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#lista-libros"><i class="icon_lg_white">@svg("plus_circle", "icon-lg")</i> Añadir</button>
                <button type="button" id="refrescarLista" class="btn btn-info"><i class="icon_lg_white">@svg("refresh_icon", "icon-lg")</i> Refrescar</button>

            </div>
            <table class="table table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>Titulo</th>
                        <th>Editorial</th>
                        <th>Fecha publicacion</th>
                        <th>Precio</th>
                        <th>Ver</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody id="librosAutorBody">
                </tbody>
            </table>
        </form>
    </div>
    @include('modals.autores.lista-libros')

    <script>
        var urlListar_libros = "{{ route('libros.table') }}";
        var urlAgregar_libro = "{{ route('autores.nuevo-libro', $autor->id) }}";
        var urlQuitar_libro = "{{ route('autores.eliminar-libro', $autor->id) }}";

        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navAutores').addClass('active border rounded');

            //refresca lista de los libros del autor en detalle
            $("#refrescarLista").trigger( "click" );
            
            $('#libros_registrados-table').DataTable({
                "lengthMenu": [[5,10,30, -1], [5,10,30, "Todos"]],
                "processing": true,
                "serverSide": true,
                "ajax": ( {
                    url: urlListar_libros,
                    pages: 5 // number of pages to cache
                }),
                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'titulo', name: 'titulo'},
                    {data: 'editorial', name: 'editorial'},
                    {data: 'fecha_publicacion', name: 'fecha_publicacion'},
                    {data: 'id', name: 'id',
                        render: function(data, type, row, meta){
                            return '<button class="btn btn-info agregarLibro" data-id="'+row.id+'"><i class="icon_lg_white">@svg("plus_circle", "icon-lg")</i> Agregar</button>'
                        }
                    },
                ]
            });
        });


        //Muesta la lista de los libros del autor en detalle
        $("#refrescarLista").on('click', function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }   
            });

            $("#librosAutorBody tr").remove();
            $.getJSON("{{route('autores.refrescar-libros', $autor->id)}}", function (data) {
                let libro_data = " ";
                $.each(data.libros, function(index, element){
                    let idLibro = data.libros[index].id;
                    let urlDetalleLibro = "{{route('libros.show')}}/"+idLibro;
                    libro_data += '<tr>';
                    libro_data += '<td>'+data.libros[index].id+'</td>';
                    libro_data += '<td>'+data.libros[index].titulo+'</td>';
                    libro_data += '<td>'+data.libros[index].editorial+'</td>';
                    libro_data += '<td>'+data.libros[index].fecha_publicacion+'</td>';
                    libro_data += '<td>'+data.libros[index].precio_venta+'</td>';
                    libro_data += '<td><a class="btn btn-info" href='+urlDetalleLibro+'><i class="icon_lg_white">@svg("eye", "icon-lg")</i> Ver</a></td>';
                    libro_data += '<td><button class="remover_libro_autor btn btn-warning" data-id="'+data.libros[index].id+'"><i class="">@svg("remove_two_icon", "icon-lg")</i> Quitar</buttos></td>';
                    libro_data += '<tr>';
                    urlDetalleLibro = " "; idAutor = " ";
                });
                $('#librosAutorBody').append(libro_data);

                //Eliminar el autor de un libro
                $('.remover_libro_autor').on('click', function (e) {
                    e.preventDefault();
                    let id = "{{$autor->id}}";
                    let libro_id = $(this).attr("data-id");
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: urlQuitar_libro,
                        method: 'post',
                        data: {
                            id: id,
                            libro_id: libro_id
                        },
                        success:function(data){
                            $("#refrescarLista").trigger( "click" );
                        }
                    });
                });
            });
        });




        $(".inputData").change( function() {
            $('#guardarCambios').prop('disabled', false);
        });

        //agregar libro al autor en detalle
        $('#libros_registrados-table tbody').on( 'click', 'button', function (e) {
            let id = "{{$autor->id}}";
            let libro_id = $(this).attr("data-id");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: urlAgregar_libro,
                method: 'post',
                data: {
                    id: id,
                    libro_id: libro_id
                },
            });
            $("#refrescarLista").trigger( "click" );
        });


        $("#eliminarAutor").on('click', function(e) {
            e.preventDefault();
            
            Swal.fire({
                title: 'Eliminarlo?',
                text: "¡Posiblemente este dato se pierda para siempre!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, eliminalo'
                }).then((result) => {
                if (result.value) {
                    Swal.fire(
                    'Eliminado exitosamente!',
                    'Usted sera redirigido al inicio.',
                    'success'
                    );
                    $.ajax({
                        url: '{{ route("autores.delete", $autor->id)}}',
                        method: 'delete',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    window.location.href = '{{ route("autores.index")}}'; 
                }
            });
        });
    </script>
@endsection