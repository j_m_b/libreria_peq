@extends('layouts.app')

@section('content')
    <div class="container">
    <form action="{{ route('autores.store') }}" method="POST" enctype="multipart/form-data">
            <div><h1 class="h1">Agregar autor</h1></div>
            {{ csrf_field() }}
            <div class="form-group row">
                <label for="inputNombre" class="col-sm-2 col-form-label">Nombre</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="inputNombre" name="nombre" placeholder="Nombres del autor">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputApellido" class="col-sm-2 col-form-label">Apellido</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="inputApellido" name="apellido" placeholder="Apellidos del autor">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputFechaNacimiento" class="col-sm-2 col-form-label">Fecha nacimiento</label>
                <div class="col-sm-10">
                <input type="date" class="form-control" id="inputFechaNacimiento" name="fecha_nacimiento" placeholder="Ingrese Fecha nacimiento">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputFechaMuerte" class="col-sm-2 col-form-label">Fecha Muerte</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" id="inputFechaMuerte" name="fecha_muerte" placeholder="Ingrese Fecha muerte">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                <button type="submit" class="btn btn-primary"><i class="icon_lg_white">@svg("save_icon", "icon-lg")</i> Siguiente</button>
                </div>
            </div>
        </form>
    </div>


    <script>
        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navAutores').addClass('active border rounded');
        });
    </script>
@endsection