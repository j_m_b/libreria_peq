@extends('layouts.app')

@section('title', 'Autores')

@section('content')
    <div class="container">
        <div>
            <h2 class="h2 text-center">Lista de autores</h2>
        </div>
        <div>
            <a class="btn btn-dark" href="{{ route('autores.create') }}"><i class="icon_lg_white">@svg("add_person_icon", "icon-lg")</i> Agregar autor</a>
        </div>
        <div class="content">
             <div>
                 @include('autores.table')
             </div>
        </div>
    </div>
    <script>
            var urlAutores = "{{ route('autores.table') }}";
            var urlVerAutor = "{{ route('autores.show') }}/";

            $(document).ready( function () {
                $(".nav li").removeClass("active border rounded");
                $('.navAutores').addClass('active border rounded');

                $('#autores_lista-table').DataTable({
                    "lengthMenu": [[5,10,30, -1], [5,10,30, "Todos"]],
                    "processing": true,
                    "serverSide": true,
                    "ajax": ( {
                        url: urlAutores,
                        pages: 5 // number of pages to cache
                    }),
                    "columns": [
                        {data: 'id', name: 'id'},
                        {data: 'nombre', name: 'nombre'},
                        {data: 'apellido', name: 'apellido'},
                        {data: 'fecha_nacimiento', name: 'fecha_nacimiento'},
                        {data: 'fecha_muerte', name: 'fecha_muerte'},
                        {data: 'libros.length', name: 'libros'},
                        {data: 'updated_at', name: 'updated_at'},
                        {data: 'id', name: 'id',
                            render: function(data, type, row, meta){
                                return '<a class="btn btn-info" href="'+urlVerAutor+row.id+'"><i class="icon_lg_white">@svg("eye", "icon-lg")</i> Ver</a>'
                            }
                        },
                    ]
                });
            });
    </script>

@endsection

@section('modals')
@endsection


