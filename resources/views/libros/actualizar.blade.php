@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('libros.update',$libro->id) }}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}
            <div><h1 class="h1">Detalles del libro #{{$libro->id}}</h1></div>

            <div class="form-group row">
                <label for="inputTitulo" class="col-sm-2 col-form-label">Titulo</label>
                <div class="col-sm-10">
                <input type="text" class="form-control inputData" id="inputTitulo" name="titulo" value="{{$libro->titulo}}" placeholder="Libro">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputEditorial" class="col-sm-2 col-form-label">Editorial</label>
                <div class="col-sm-10">
                <input type="text" class="form-control inputData" id="inputEditorial" name="editorial" value="{{$libro->editorial}}" placeholder="Ingrese editorial">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputFechaPublicacion" class="col-sm-2 col-form-label">Fechap publicacion</label>
                <div class="col-sm-10">
                <input type="date" class="form-control inputData" id="inputFechaPublicacion" name="fecha_publicacion" value="{{$libro->fecha_publicacion}}" placeholder="Ingrese Fecha publicacion">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPrecio" class="col-sm-2 col-form-label">Precio</label>
                <div class="col-sm-10">
                <input type="number" class="form-control inputData" id="inputPrecio" name="precio_venta" value="{{$libro->precio_venta}}" placeholder="Ingrese valor del producto sin puntos decimales">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPrecioMinorista" class="col-sm-2 col-form-label">Precio minorista</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control inputData" id="inputPrecioMinorista" name="precio_minorista" value="{{$libro->precio_minorista}}" placeholder="Ingrese valor del producto sin puntos decimales">
                </div>
            </div>
            <fieldset class="form-group">
                <div class="row">
                    <legend class="col-form-label col-sm-2 pt-0">Valoracion</legend>
                    <div class="col-sm-10">
                        @foreach ($valoracion as $item)                        
                        <div class="form-check">
                            @if ($item->id == $libro->valoracion_id)
                                <input class="form-check-input inputData" type="radio" name="valoracion_id" id="gridRadios{{$item->id}}" value="{{$item->id}}" checked>
                            @else
                                <input class="form-check-input inputData" type="radio" name="valoracion_id" id="gridRadios{{$item->id}}" value="{{$item->id}}">                            
                            @endif
                            <label class="form-check-label" for="gridRadios{{$item->id}}">
                                {{$item->valoracion}}
                            </label>
                        </div>
                        @endforeach
                    </div>
                </div>
            </fieldset>
            <div class="form-group row">
                <label for="inputPrecio" class="col-sm-2 col-form-label">Descripcion</label>
                <div class="col-sm-10">
                    <textarea class="form-control inputData" id="inputPrecio" name="descripcion_valoracion" rows="3" value="{{$libro->descripcion_valoracion}}" placeholder="Ingrese breve descripcion del estado del producto u alguna otra nota">{!! $libro->descripcion_valoracion !!}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputImgActual" class="col-sm-2 col-form-label">Portada actual</label>
                <div class="col-sm-10">
                    <img style="width: 25%;" id="inputImgActual" src="{{ asset('storage/'.$libro->imagen) }}" alt="{{$libro->titulo}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputImg" class="col-sm-2 col-form-label">Cambiar imagen de portada</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" id="inputImg" name="imagen" value="{{ asset('storage/'.$libro->imagen) }}" alt="{{$libro->titulo}}" placeholder="Titulo del libro">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button id="guardarCambios" type="submit" class="btn btn-primary" disabled><i class="icon_lg_white">@svg("save_icon", "icon-lg")</i> Guardar</button>
                    <a id="eliminarLibro" class="btn btn-danger" href="#"> <i class="icon_lg_white">@svg("delete_icon", "icon-lg")</i> Eliminar</a>
                </div>
            </div>
        </form>
        <form action="">
            <div>
                <h1 class="h1">Autores </h1>
                <button type="button" id="" class="btn btn-info" data-toggle="modal" data-target="#lista-autores"><i class="icon_lg_white">@svg("add_person_icon", "icon-lg")</i> Añadir</button>
                <button type="button" id="refrescarLista" class="btn btn-info"><i class="icon_lg_white">@svg("refresh_icon", "icon-lg")</i> Refrescar</button>

            </div>
            <table id="autoresLibro" class="table table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Fecha nacimiento</th>
                        <th scope="col">Fecha defunción</th>
                        <th scope="col">Ver</th>
                        <th scope="col">Eliminar</th>
                    </tr>
                </thead>
                <tbody id="autoresLibroBody">
                </tbody>
            </table>

        </form>
    </div>
    @include('modals.libros.lista-autores')

    <script>
        var urlListar_autores = "{{ route('autores.table') }}";
        var urlAgregar_autor = "{{ route('libros.nuevo-autor', $libro->id) }}";
        var urlQuitar_autor = "{{ route('libros.eliminar-autor', $libro->id) }}";




        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navLibros').addClass('active border rounded');

            //refresca lista de los autores del libro en detalle
            $("#refrescarLista").trigger( "click" );

            //Lista de todos los autores
            $('#autores_registrados-table').DataTable({
                "lengthMenu": [[5,10,30, -1], [5,10,30, "Todos"]],
                "processing": true,
                "serverSide": true,
                "ajax": ( {
                    url: urlListar_autores,
                    pages: 5 // number of pages to cache
                }),
                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'nombre', name: 'nombre'},
                    {data: 'apellido', name: 'apellido'},
                    {data: 'id', name: 'id',
                        render: function(data, type, row, meta){
                            return '<button class="btn btn-info agregarAutor" data-id="'+row.id+'"><i class="icon_lg_white">@svg("add_person_icon", "icon-lg")</i> Agregar</button>'
                        }
                    },
                ]
            });
        });


        //Muesta la lista de los autores del libro en detalle
        $("#refrescarLista").on('click', function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }   
            });
            $("#autoresLibroBody tr").remove();
            $.getJSON("{{route('libros.refrescar-autores', $libro->id)}}", function (data) {
                let autor_data = " ";
                $.each(data.autores, function(index, element){
                    let idAutor = data.autores[index].id;
                    let urlDetalleAutor = "{{route('autores.show')}}/"+idAutor;
                    autor_data += '<tr>';
                    autor_data += '<td>'+data.autores[index].id+'</td>';
                    autor_data += '<td>'+data.autores[index].nombre+'</td>';
                    autor_data += '<td>'+data.autores[index].apellido+'</td>';
                    autor_data += '<td>'+data.autores[index].fecha_nacimiento+'</td>';
                    autor_data += '<td>'+data.autores[index].fecha_muerte+'</td>';
                    autor_data += '<td><a class="btn btn-info" href='+urlDetalleAutor+'><i class="icon_lg_white">@svg("eye", "icon-lg")</i> Ver</a></td>';
                    autor_data += '<td><button class="remover_autor_libro btn btn-warning" data-id="'+data.autores[index].id+'"> <i class="">@svg("remove_two_icon", "icon-lg")</i> Quitar</buttos></td>';
                    autor_data += '<tr>';
                    urlDetalleAutor = " "; idAutor = " ";
                });
                $('#autoresLibroBody').append(autor_data);

                //Eliminar el autor de un libro
                $('.remover_autor_libro').on('click', function (e) {
                    e.preventDefault();
                    let id = "{{$libro->id}}";
                    let autor_id = $(this).attr("data-id");
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: urlQuitar_autor,
                        method: 'post',
                        data: {
                            id: id,
                            autor_id: autor_id
                        },
                        success:function(data){
                            $("#refrescarLista").trigger( "click" );
                        }
                    });
                });
            });
        });


        

        //agregar autor al libro en detalle
        $('#autores_registrados-table tbody').on( 'click', 'button', function (e) {
            let id = "{{$libro->id}}";
            let autor_id = $(this).attr("data-id");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: urlAgregar_autor,
                method: 'post',
                data: {
                    id: id,
                    autor_id: autor_id
                },
            });
            $("#refrescarLista").trigger( "click" );
        });


        //habilitar el boton de guardar cuando se modifique informacion del libro en detalle
        $(".inputData").change( function() {
            $('#guardarCambios').prop('disabled', false);
        });


        //Elimina el libro en detalle de la lista de libros
        $("#eliminarLibro").on('click', function(e) {
            e.preventDefault();
            
            Swal.fire({
                title: 'Eliminarlo?',
                text: "¡Posiblemente este dato se pierda para siempre!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, eliminalo'
                }).then((result) => {
                if (result.value) {
                    Swal.fire(
                    'Eliminado exitosamente!',
                    'Usted sera redirigido al inicio.',
                    'success'
                    );
                    $.ajax({
                        url: '{{ route("libros.delete", $libro->id)}}',
                        method: 'delete',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    window.location.href = '{{ route("libros.index")}}'; 
                }
            });
        });
    </script>
@endsection