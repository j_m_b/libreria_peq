@extends('layouts.app')

@section('content')
    <div class="container">
    <form action="{{ route('libros.store') }}" method="POST" enctype="multipart/form-data">
            <div><h1 class="h1">Agregar nuevo libro</h1></div>
            {{ csrf_field() }}
            <div class="form-group row">
                <label for="inputTitulo" class="col-sm-2 col-form-label">Titulo</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="inputTitulo" name="titulo" placeholder="Libro">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputEditorial" class="col-sm-2 col-form-label">Editorial</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="inputEditorial" name="editorial" placeholder="Ingrese editorial">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputFechaPublicacion" class="col-sm-2 col-form-label">Fechap publicacion</label>
                <div class="col-sm-10">
                <input type="date" class="form-control" id="inputFechaPublicacion" name="fecha_publicacion" placeholder="Ingrese Fecha publicacion">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPrecio" class="col-sm-2 col-form-label">Precio</label>
                <div class="col-sm-10">
                <input type="number" class="form-control" id="inputPrecio" name="precio_venta" placeholder="Ingrese valor del producto sin puntos decimales">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPrecioMinorista" class="col-sm-2 col-form-label">Precio minorista</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" id="inputPrecioMinorista" name="precio_minorista" placeholder="Ingrese valor del producto sin puntos decimales">
                </div>
            </div>
            <fieldset class="form-group">
                <div class="row">
                    <legend class="col-form-label col-sm-2 pt-0">Valoracion</legend>
                    <div class="col-sm-10">
                        @foreach ($valoracion as $item)                        
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="valoracion_id" id="gridRadios{{$item->id}}" value="{{$item->id}}">
                            <label class="form-check-label" for="gridRadios{{$item->id}}">
                                {{$item->valoracion}}
                            </label>
                        </div>
                        @endforeach
                    </div>
                </div>
            </fieldset>
            <div class="form-group row">
                <label for="inputDescripcion" class="col-sm-2 col-form-label">Descripcion</label>
                <div class="col-sm-10">
                    <textarea rows="3" class="form-control" id="inputDescripcion" name="descripcion_valoracion" rows="3" placeholder="Ingrese breve descripcion del estado del producto u alguna otra nota"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputImg" class="col-sm-2 col-form-label">Portada</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" id="inputImg" name="imagen" placeholder="Imagen de la portada del libro">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                <button type="submit" class="btn btn-primary"><i class="icon_lg_white">@svg("save_icon", "icon-lg")</i> Siguiente</button>
                </div>
            </div>
        </form>
    </div>
    
    <script>
        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navLibros').addClass('active border rounded');
        });
    </script>
@endsection