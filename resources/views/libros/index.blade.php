@extends('layouts.app')

@section('title', 'Libros')

@section('content')
    <div class="container">
        <div>
            <h2 class="h2 text-center">Nuestros libros</h2>
        </div>
        <div>
            <a class="btn btn-dark" href="{{ route('libros.create') }}"><i class="icon_lg_white">@svg("plus_circle", "icon-lg")</i> Agregar Libro</a>
        </div>
        <div class="content">
             <div>
                 @include('libros.table')
             </div>
        </div>
    </div>
    <script>
            var urlLibros = "{{ route('libros.table') }}";
            var urlVerLibro = "{{ route('libros.show') }}/";

            $(document).ready( function () {
                $(".nav li").removeClass("active border rounded");
                $('.navLibros').addClass('active border rounded');

                $('#libros_lista-table').DataTable({
                    "lengthMenu": [[5,10,30, -1], [5,10,30, "Todos"]],
                    "processing": true,
                    "serverSide": true,
                    "ajax": ( {
                        url: urlLibros,
                        pages: 5 // number of pages to cache
                    }),
                    "columns": [
                        {data: 'id', name: 'id'},
                        {data: 'titulo', name: 'titulo'},
                        {data: 'editorial', name: 'editorial'},
                        {data: 'fecha_publicacion', name: 'fecha_publicacion'},
                        {data: 'precio_venta', name: 'precio_venta'},
                        {data: 'precio_minorista', name: 'precio_minorista'},
                        {data: 'valoracion.valoracion', name: 'valoracion.valoracion'},
                        {data: 'updated_at', name: 'updated_at'},
                        {data: 'id', name: 'id',
                            render: function(data, type, row, meta){
                                return '<a class="btn btn-info" href="'+urlVerLibro+row.id+'"><i class="icon_lg_white">@svg("eye", "icon-lg")</i> Ver</a>'
                            }
                        },
                    ]
                });
            });
    </script>

@endsection

@section('modals')
@endsection


