@extends('layouts.app')

@section('title', 'blog')

@section('content')
    <div class="container">
        <div>
            <h2 class="h2 text-center">Blogs</h2>
        </div>
        <div>
            <a class="btn btn-dark" href="{{route('blog.create')}}"><i class="icon_lg_white">@svg("plus_circle", "icon-lg")</i> Nuevo blog</a>
            <a class="btn btn-dark" href="{{route('blog.categorias.index')}}"><i class="icon_lg_white">@svg("cathe_icon", "icon-lg")</i> Categorias</a>
        </div>
        <div>
            @include('blog.table')
        </div>
    </div>

    <script>
        var urlBlogs = "{{ route('blog.table') }}";
        var urlVerBlog = "{{ route('blog') }}/";

        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navBlog').addClass('active border rounded');

            $('#blog_lista-table').DataTable({
                "lengthMenu": [[5,10,30, -1], [5,10,30, "Todos"]],
                "processing": true,
                "serverSide": true,
                "ajax": ( {
                    url: urlBlogs,
                    pages: 5 // number of pages to cache
                }),
                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'titulo', name: 'titulo'},
                    {data: 'categorias', name: 'categorias',
                        render: function(data, type, row, meta){
                            let cats = ' ';
                                $.each(data, function(i, item) {
                                    cats += '<h6><span class="badge badge-primary">'+row.categorias[i].categoria+'</span></h6>';
                                });
                            return cats;
                        }
                    },
                    {data: 'empleado.name', name: 'empleado.name'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'estado', name: 'estado',
                        render: function(data, type, row, meta){
                            if (row.estado == 1) {
                                return '<input class="estado_blog" type="checkbox" value="'+row.id+'" checked>';
                            } else {
                                return '<input class="estado_blog" type="checkbox" value="'+row.id+'">';                                
                            }
                        }
                    },
                    {data: 'id', name: 'id',
                        render: function(data, type, row, meta){
                            return '<a class="btn btn-info" href="'+urlVerBlog+row.slug+'"><i class="icon_lg_white">@svg("eye", "icon-lg")</i> Ver</a>'
                        }
                    },
                ]
            });

            $('#blog_lista-table tbody').on('change', '.estado_blog', function () {
                let id = $(this).val();
                let urlCambiarEstado = "{{route('blog.update.estado')}}/"+id;
                //alert(id);	
                //$.post(urlCambiarEstado);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "put",
                    url: urlCambiarEstado,
                });
            });
        });
    
    </script>
@endsection