@extends('layouts.app')

@section('title', 'blog')

@section('content')
    <div class="container">
        <div><h1 class="h1">Agregar blog</h1></div>
        <div>
            <form action="{{route('blog.store')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label for="inputTitulo" class="col-sm-2 col-form-label">Titulo</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputTitulo" name="titulo" placeholder="Titulo del blog">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputImg" class="col-sm-2 col-form-label">Imagen</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" id="inputImg" name="imagen" placeholder="Imagen del blog">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="summernote" class="col-sm-2 col-form-label">Contenido</label>
                    <div class="col-sm-10">
                        <textarea id="summernote" name="contenido"></textarea>                        
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <button class="btn btn-primary" type="submit"><i class="icon_lg_white">@svg("save_icon", "icon-lg")</i> Generar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <script>
        $(document).ready(function() {
            $(".nav li").removeClass("active border rounded");
            $('.navBlog').addClass('active border rounded');

            $('#summernote').summernote({
                height: 300,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: true                  // set focus to editable area after initializing summernote
            });
        });
    </script>
@endsection