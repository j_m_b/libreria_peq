@extends('layouts.app')

@section('title', 'blog')

@section('content')

    <div class="container">
        <div>
            <div>
                <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#crearCategoria">Crear categoria</button>
            </div>
            <table class="table table-dark table-hover">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Editar</th>
                    <th scope="col">Eliminar</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($categorias as $categoria) 
                    <tr>
                        <th scope="row">{{$categoria->id}}</th>
                        <td>{{$categoria->categoria}}</td>
                        <td><button type="button" data-id="{{$categoria->id}}" data-categoria="{{$categoria->categoria}}" class="btn btn-warning editar_categoria" data-toggle="modal" data-target="#editarCategoria">Editar</button></td>
                        <td><button type="button" data-id="{{$categoria->id}}" class="btn btn-danger eliminar_categoria">Eliminar</button></td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
        </div>

        @include('modals.blog.crear_categoria')
        @include('modals.blog.editar_categoria')
    </div>

    <script>
        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navBlog').addClass('active border rounded');
        });

        $('.editar_categoria').on('click', function(){
            let urlEditarCategoria = "{{route('blog.categorias.update')}}/"+$(this).attr('data-id');

            $('#editar-categoria-form').attr('action', urlEditarCategoria);

            $( ".titulo-categoria" ).empty();
            $( ".titulo-categoria" ).text('Editar categoria '+$(this).attr('data-id'));

            $("#inputCategoriaEditar").empty();
            $("#inputCategoriaEditar").val($(this).attr('data-categoria'));
            
        });

        $(".eliminar_categoria").on('click', function(e) {
            e.preventDefault();
            let id = $(this).attr('data-id');
            let urlEliminarCategoria = "{{route('blog.categorias.delete')}}/"+id;
            
            Swal.fire({
                title: 'Eliminarla?',
                text: "¡Posiblemente este dato se pierda para siempre!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, eliminala'
                }).then((result) => {
                if (result.value) {
                    Swal.fire(
                    'Eliminada exitosamente!',
                    'Esta categoria ya no existe.',
                    'success'
                    );
                    $.ajax({
                        url: urlEliminarCategoria,
                        method: 'delete',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    window.location.href = '{{ route("blog.categorias.index")}}'; 
                }
            });
        });

    </script>

@endsection