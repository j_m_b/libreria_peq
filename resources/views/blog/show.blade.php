@extends('layouts.app')

@section('title', 'blog')

@section('content')
    <div class="container">
        <div>
            <h1 class="h1">Editar blog: <a class="" href="{{route('blog',$blog->slug)}}" target="_blank">Ver<span class="sr-only">(current)</span></a></h1>
        </div>
        <div>
            <form action="{{route('blog.update',$blog->id)}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="portada_actual" value="{{$blog->imagen}}">
                <div class="form-group row">
                    <label for="inputTitulo" class="col-sm-2 col-form-label">Titulo</label>
                    <div class="col-sm-10">
                        <input value="{{$blog->titulo}}" type="text" class="form-control" id="inputTitulo" name="titulo" placeholder="Titulo del blog">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputImgActual" class="col-sm-2 col-form-label">Portada actual</label>
                    <div class="col-sm-10">
                        <img style="width: 25%;" id="inputImgActual" src="{{ asset('storage/'.$blog->imagen) }}" alt="{{$blog->titulo}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputImg" class="col-sm-2 col-form-label">Cambiar imagen de portada</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" id="inputImg" name="imagen" value="{{ asset('storage/'.$blog->imagen) }}" alt="{{$blog->titulo}}" placeholder="Titulo del blog">
                    </div>
                </div>
                <div class="form-group row">
                <label for="inputCategorias" class="col-sm-2 col-form-label"><button type="button" id="inputCategorias" class="btn btn-dark" data-toggle="modal" data-target="#agregarCategoria"><i class="icon_lg_white">@svg("cathe_icon", "icon-lg")</i> Categorias</button></label>
                    <div class="col-sm-10">
                    @foreach ($blog->categorias as $categoria)
                        <h6><span class="badge badge-primary">{{$categoria->categoria}}</span></h6>
                    @endforeach
                    </div>
                </div>
                <div class="form-group row">
                    <label for="summernote" class="col-sm-2 col-form-label">Contenido</label>
                    <div class="col-sm-10">
                        <textarea id="summernote" name="contenido">{{$blog->contenido}}</textarea>                        
                    </div>
                </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <button class="btn btn-primary" type="submit"><i class="icon_lg_white">@svg("save_icon", "icon-lg")</i> Editar</button>
                        <button class="btn btn-danger" data-id="{{$blog->id}}" id="eliminar_blog" type="submit"><i class="icon_lg_white">@svg("delete_icon", "icon-lg")</i> Eliminar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @include('modals.blog.agregar_categoria')
  
    <script>
        $(document).ready(function() {
            $(".nav li").removeClass("active border rounded");
            $('.navBlog').addClass('active border rounded');

            $('#summernote').summernote({
                height: 300,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: true                  // set focus to editable area after initializing summernote
            });


            $("#eliminar_blog").on('click', function(e) {
                e.preventDefault();
                let id = $(this).attr('data-id');
                let urlEliminarCategoria = "{{route('blog.delete')}}/"+id;
                
                Swal.fire({
                    title: 'Eliminarlo?',
                    text: "¡Posiblemente este dato se pierda para siempre!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminalo'
                    }).then((result) => {
                    if (result.value) {
                        Swal.fire(
                        'Eliminado exitosamente!',
                        'Este blog ya no existe.',
                        'success'
                        );
                        $.ajax({
                            url: urlEliminarCategoria,
                            method: 'delete',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        window.location.href = '{{ route("blog.index")}}'; 
                    }
                });
            });

        });
    </script>

@endsection