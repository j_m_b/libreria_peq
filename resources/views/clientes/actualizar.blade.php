@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('clientes.update',$cliente->id) }}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT">
            
            {{ csrf_field() }}
            <div><h1 class="h1">Detalles del cliente #{{$cliente->id}}</h1></div>
            <div class="form-group row">
                <label for="inputNombre" class="col-sm-2 col-form-label">Nombres</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control inputData" id="inputNombre" name="nombre" value="{{$cliente->nombre}}" placeholder="Nombres">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputApellido" class="col-sm-2 col-form-label">Apellidos</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control inputData" id="inputApellido" name="apellido" value="{{$cliente->apellido}}" placeholder="Apellidos">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputNumTelefono" class="col-sm-2 col-form-label">Telefono</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" id="inputNumTelefono" name="num_telefono" value="{{$cliente->num_telefono}}" placeholder="Telefono del cliente">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputDireccionEnvio" class="col-sm-2 col-form-label">Direccion de envio</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputDireccionEnvio" name="direccion_envio" value="{{$cliente->direccion_envio}}" placeholder="Direccion de envio del cliente">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail" name="email" value="{{$cliente->email}}" placeholder="Correo electronico del cliente">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                <button id="guardarCambios" type="submit" class="btn btn-primary" disabled><i class="icon_lg_white">@svg("save_icon", "icon-lg")</i> Guardar</button>
                <a id="eliminarCliente" class="btn btn-danger" href="#"><i class="icon_lg_white">@svg("delete_icon", "icon-lg")</i> Eliminar</a>
                </div>
            </div>
        </form>
        <form action="">
            <div>
                <h1 class="h1">Compras </h1>
                <a class='btn btn-info' href='{{ route('ventas.create', $cliente->id) }}'><i class="icon_lg_white">@svg("carrito_icon", "icon-lg")</i>Agrgar venta</a>
            </div>
            <table id="compras_usuario-table" class="table display compact nowrap" style="width:100%">
                <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>Registro</th>
                        <th>Total</th>
                        <th>Fecha registro</th>
                        <th>Vendedor</th>
                        <th>Informacion</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </form>
        @include('modals.ventas.libros-registro')
    </div>

    <script>
        var id_cliente = '{{$cliente->id}}';
        var urlListarCompras = "{{ route('ventas.table-usuario') }}/";
        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navClientes').addClass('active border rounded');

            $('#compras_usuario-table').DataTable({
                "lengthMenu": [[5,10,30, -1], [5,10,30, "Todos"]],
                "processing": true,
                "serverSide": true,
                "ajax": ( {
                    url: urlListarCompras+id_cliente,
                    pages: 5 // number of pages to cache
                }),
                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'registro', name: 'registro'},
                    {data: 'total', name: 'total'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'id', name: 'id'},
                    {data: 'id', name: 'id',
                        render: function(data, type, row, meta){
                            return '<button type="button" data-id="'+row.id+'" data-registro="'+row.registro+'" data-total="'+row.total+'" data-fecha="'+row.created_at+'" class="btn btn-info info_venta" data-toggle="modal" data-target="#detalle_registro_compra" ><i class="icon_lg_white">@svg("eye", "icon-lg")</i> Ver</button>'
                        }
                    },
                ],
            });



            $('#compras_usuario-table tbody').on('click', 'button.info_venta', function (e){
                e.preventDefault();
                //limpio modal
                $( "#clienteCompra" ).empty();
                $( "#registroCompra" ).empty();
                $( "#fechaCompra" ).empty();
                $( "#vendedorCompra" ).empty();
                $( "#totalCompra" ).empty();
                $( "#librosTablaCompra" ).empty();
                //Variables
                let registro_id = $(this).attr('data-id');
                let registro = $(this).attr('data-registro');
                let total = $(this).attr('data-total');
                let fecha_registro = $(this).attr('data-fecha');
                //reasigno valores
                $( "#registroCompra" ).text(registro);
                $( "#fechaCompra" ).text(fecha_registro);
                $( "#totalCompra" ).text('$'+total);
                let urlDetalleCompra = "{{route('ventas.show')}}/"+registro_id;
                let urlDetalleLibro = "{{route('libros.show')}}/";

                $.getJSON(urlDetalleCompra, function (data) {
                    $( "#vendedorCompra" ).text(data[0].empleado.name);
                    let urlCliente = "{{route('clientes.show')}}/"+data[0].cliente_id;
                    $( "#clienteCompra" ).append('<a href="'+urlCliente+'">'+data[0].cliente.nombre+' '+data[0].cliente.apellido+'</a>');
                    let libros_data = " ";
                    $.each(data, function(index, element){
                        libros_data += '<tr>';
                        libros_data += '<td><a href="'+urlDetalleLibro+data[index].libro.id+'">'+data[index].libro.id+'</a></td>';
                        libros_data += '<td>'+data[index].libro.titulo+'</td>';
                        libros_data += '<td><ul>';
                        $.each(data[index].libro.autores, function(i, e){                        
                            libros_data += "<li>"+data[index].libro.autores[i].nombre+" "+data[index].libro.autores[i].apellido+"</li>";
                        });
                        libros_data += '</ul></td>';
                        libros_data += '<td>$'+data[index].libro.precio_venta+'</td>';
                        libros_data += '<td>'+data[index].cantidad+'</td>';
                        libros_data += '<tr>';
                    });
                    $('#librosTablaCompra').append(libros_data);
                });
            });
        });


        $(".inputData").change( function() {
            $('#guardarCambios').prop('disabled', false);
        });


        $("#eliminarCliente").on('click', function(e) {
            e.preventDefault();
            
            Swal.fire({
                title: 'Eliminarlo?',
                text: "¡Posiblemente este dato se pierda para siempre!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, eliminalo'
                }).then((result) => {
                if (result.value) {
                    Swal.fire(
                    'Eliminado exitosamente!',
                    'Usted sera redirigido al inicio.',
                    'success'
                    );
                    $.ajax({
                        url: '{{ route("clientes.delete", $cliente->id)}}',
                        method: 'delete',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    window.location.href = '{{ route("clientes.index")}}'; 
                }
            });
        });
    </script>
@endsection