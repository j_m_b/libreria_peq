@extends('layouts.app')

@section('title', 'Clientes')

@section('content')
    <div class="container">
        <div>
            <h2 class="h2 text-center">Nuestros clientes</h2>
        </div>
        <div>
            <a class="btn btn-dark" href="{{ route('clientes.create') }}"><i class="icon_lg_white">@svg("add_person_icon", "icon-lg")</i> Agregar clientes</a>
        </div>
        <div class="content">
             <div>
                 @include('clientes.table')
             </div>
        </div>
    </div>
    <script>
        var urlClientes = "{{ route('clientes.table') }}";
        var urlVerCliente = "{{ route('clientes.show') }}/";

        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navClientes').addClass('active border rounded');

            $('#clientes_lista-table').DataTable({
                "lengthMenu": [[5,10,30, -1], [5,10,30, "Todos"]],
                "processing": true,
                "serverSide": true,
                "ajax": ( {
                    url: urlClientes,
                    pages: 5 // number of pages to cache
                }),
                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'nombre', name: 'nombre'},
                    {data: 'apellido', name: 'apellido'},
                    {data: 'num_telefono', name: 'num_telefono'},
                    {data: 'direccion_envio', name: 'direccion_envio'},
                    {data: 'email', name: 'email'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'id', name: 'id',
                        render: function(data, type, row, meta){
                            return '<a class="btn btn-info" href="'+urlVerCliente+row.id+'"><i class="icon_lg_white">@svg("eye", "icon-lg")</i> Ver</a>';                            
                        }
                    },
                ]
            });
        });
    </script>

@endsection

@section('modals')
@endsection


