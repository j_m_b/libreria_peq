@extends('layouts.app')

@section('content')
    <div class="container">
    <form action="{{ route('clientes.store') }}" method="POST" enctype="multipart/form-data">
            <div><h1 class="h1">Agregar cliente</h1></div>
            {{ csrf_field() }}
            <div class="form-group row">
                <label for="inputNombre" class="col-sm-2 col-form-label">Nombre</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputNombre" name="nombre" placeholder="Nombres del autor">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputApellido" class="col-sm-2 col-form-label">Apellido</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputApellido" name="apellido" placeholder="Apellidos del autor">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputNumTelefono" class="col-sm-2 col-form-label">Telefono</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" id="inputNumTelefono" name="num_telefono" placeholder="Telefono del cliente">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputDireccionEnvio" class="col-sm-2 col-form-label">Direccion de envio</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputDireccionEnvio" name="direccion_envio" placeholder="Direccion de envio del cliente">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Correo electronico del cliente">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                <button type="submit" class="btn btn-primary"> <i class="icon_lg_white">@svg("save_icon", "icon-lg")</i> Siguiente</button>
                </div>
            </div>
        </form>
    </div>

    <script>
        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navClientes').addClass('active border rounded');
        });


    </script>
@endsection