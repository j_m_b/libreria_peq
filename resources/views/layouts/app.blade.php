<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    
    {{-- <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> --}}
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>{{ config('app.name', 'Laravel') }}</title>
    
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/master.css') }}" rel="stylesheet">
    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    
    
    <!-- DataTable-jquery -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" defer></script>
    {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> --}}
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js" defer></script>
    
    <!-- fontawesome icons -->
    <script src="https://kit.fontawesome.com/dd90ed5f50.js"></script>

    
    <link href="{{ asset('vendor/laravel-admin-ext/dist/summernote.css') }}" rel="stylesheet">        
    <script src="{{ asset('vendor/laravel-admin-ext/dist/summernote.min.js') }}" defer></script>
    
    
    
</head>
<body>
    <div id="app">
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
                <a class="navbar-brand" href="{{route('inicio')}}"> <i class="icon_lg_white">@svg("icon_home", "icon-lg")</i> Libreria</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <form action="{{route('buscar')}}" method="GET" class="form-inline my-2 my-lg-0" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input class="form-control mr-sm-2" name="buscar" type="search" placeholder="¿Que libros buscas?" aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
                    </form>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest

                            <li class="nav-item">
                                <a class="nav-link navProductos" href="{{route('productos.index')}}"><i class="icon_lg_white">@svg("store_icon", "icon-lg")</i> Productos <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item navBlog">
                                <a class="nav-link" href="{{route('blogs')}}"><i class="icon_lg_white">@svg("layers_two", "icon-lg")</i> Blog <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item navFAQ">
                                <a class="nav-link" href="{{route('faq')}}"><i class="icon_lg_white">@svg("question_icon", "icon-lg")</i> FAQ <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item navAcercaDe">
                                <a class="nav-link" href="{{route('acerca-de')}}"><i class="icon_lg_white">@svg("info_icon", "icon-lg")</i> Acerca de<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item navContactenos">
                                <a class="nav-link" href="{{route('contactenos')}}"><i class="icon_lg_white">@svg("message_icon", "icon-lg")</i> Contactenos <span class="sr-only">(current)</span></a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            {{-- 
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                            --}}
                        @else

                            <li class="nav-item navVentas">
                                <a class="nav-link" href="{{route('ventas.index')}}"><i class="icon_lg_white">@svg("dollar_icon", "icon-lg")</i> Ventas <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item navClientes">
                            <a class="nav-link" href="{{route('clientes.index')}}"><i class="icon_lg_white">@svg("client_icon", "icon-lg")</i> Clientes <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item navLibros ">
                                <a class="nav-link" href="{{route('libros.index')}}"><i class="icon_lg_white">@svg("book", "icon-lg")</i> Libros <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item navAutores">
                                <a class="nav-link" href="{{route('autores.index')}}"><i class="icon_lg_white">@svg("autors_icon", "icon-lg")</i> Autores <span class="sr-only">(current)</span></a>
                            </li>
                            @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('super-admin'))
                                <li class="nav-item navCuentas">
                                    <a class="nav-link" href="{{route('cuentas.index')}}"><i class="icon_lg_white">@svg("accounts_icon", "icon-lg")</i> Administrar cuentas <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item navBlog">
                                    <a class="nav-link" href="{{route('blog.index')}}"><i class="icon_lg_white">@svg("blog_icon", "icon-lg")</i> Administrar Blog <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item navFAQ">
                                    <a class="nav-link" href="{{route('faq.index')}}"><i class="icon_lg_white">@svg("faq_icon", "icon-lg")</i> Administrar FAQ <span class="sr-only">(current)</span></a>
                                </li>
                            @endif

                            <li class="nav-item dropdown navPerfil">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{route('home')}}"><i class="">@svg("home_icon", "icon-lg")</i> Inicio</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                        <i class="">@svg("close_session_two_icon", "icon-lg")</i>
                                        Cerrar sesion
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </nav>
        </header><br><br><br>
        <main class="py-4">
            @yield('content')
            @include('sweetalert::alert')
        </main>
    </div>
    <br>
    <footer>
        <ul class="nav fixed-bottom justify-content-center bg-dark">
            <li class="nav-item">
                <a class="nav-link active a-footer" href="https://essau.co">[ESSAU.CO]</a>
            </li>
        </ul>
    </footer>
    
</body>
</html>
