@extends('layouts.app')

@section('title', 'contactenos')

@section('content')
    <div class="container">
        <div>
            <table id="mensajes_contactenos-table" class="table display compact nowrap" style="width:100%">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombres</th>
                    <th scope="col">Apellidos</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Creado el</th>
                    <th scope="col">Respondido el</th>
                    <th scope="col">Respondido por</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Ver</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    @include('modals.contactenos.revisar')

    <script>
        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navPerfil').addClass('active border rounded');

            let urlVerMensajesAll = "{{route('contactenos.all-table')}}";

            $('#mensajes_contactenos-table').DataTable({
                "lengthMenu": [[5,10,30, -1], [5,10,30, "Todos"]],
                "processing": true,
                "serverSide": true,
                "order": [[ 7, "asc" ]],
                "ajax": ( {
                    url: urlVerMensajesAll,
                    pages: 5 // number of pages to cache
                }),
                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'nombres', name: 'nombres'},
                    {data: 'apellidos', name: 'apellidos'},
                    {data: 'email', name: 'email'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'empleado.name', name: 'empleado.name'},
                    {data: 'estado', name: 'estado',
                        render: function(data, type, row, meta){
                            if (row.estado == 1) {
                                return 'resuelto';
                            } else {
                                return 'sin resolver';                                
                            }
                        }
                    },
                    {data: 'id', name: 'id',
                        render: function(data, type, row, meta){
                            return '<button type="button" data-id="'+row.id+'" class="ver-mensaje btn btn-primary" data-toggle="modal" data-target="#revisarMensaje-contactenos"><i class="icon_lg_white">@svg("eye", "icon-lg")</i> Ver</button>'
                        }
                    }
                ]
            });


            $('#mensajes_contactenos-table tbody').on('click', '.ver-mensaje', function () {
                let id = $(this).attr('data-id');
                let urlRevisarMensaje = "{{route('contactenos.show')}}/"+id;
                let urlCambiarEstadoMensaje = "{{route('contactenos.update_estado')}}/"+id;

                $(".titulo-mensaje").empty();
                $("#nombres").empty();
                $("#telefono").empty();
                $("#email").empty();
                $("#pregunta").empty();
                $("#respuesta").empty();
                $("#empleado").empty();


                $.get(urlRevisarMensaje, function( data ) {
                    if (data.estado == 1) {
                        let = estado_actual = "resuelto"
                    } else {
                        let = estado_actual = "sin resolver"
                    }
                    let estado_tag = '<h6><span class="badge badge-primary">'+estado_actual+'</span><h6>';
                    $(".titulo-mensaje").html('Mensaje #'+data.id+' '+estado_tag);
                    $("#nombres").text( data.nombres+' '+data.apellidos);
                    $("#telefono").text( data.telefono);
                    $("#email").text( data.email);
                    $("#pregunta").text( data.pregunta);
                    $("#respuesta").text( data.respuesta);
                    $("#empleado").text(' '+data.empleado.name+' el '+data.updated_at);
                    $('#responder-contactenos-form').attr('action', urlCambiarEstadoMensaje);
                    $("#eliminar_mensaje").attr('data-id', data.id);
                });
            });

            $("#eliminar_mensaje").on('click', function(e) {
                e.preventDefault();
                let id = $(this).attr('data-id');
                let urlEliminarCategoria = "{{route('contactenos.delete')}}/"+id;
                
                Swal.fire({
                    title: 'Eliminarlo?',
                    text: "¡Posiblemente este dato se pierda para siempre!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminala'
                    }).then((result) => {
                    if (result.value) {
                        Swal.fire(
                        'Eliminado exitosamente!',
                        'Este mensaje ya no existe.',
                        'success'
                        );
                        $.ajax({
                            url: urlEliminarCategoria,
                            method: 'delete',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        window.location.href = '{{ route("contactenos.all")}}'; 
                    }
                });
            });
        });
    </script>
@endsection