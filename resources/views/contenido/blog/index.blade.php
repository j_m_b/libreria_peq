@extends('layouts.app')

@section('title', 'blog')

@section('content')

    <div class="container">
        <h2 class="h2 text-center"><strong>Nuestro blog</strong></h2>
        
        <div class="card-columns">
            @foreach ($blogs as $blog)
            @if ($blog->estado)    
                <div class="card" style="width: 18rem;">
                    <a href="{{ route('blog',$blog->slug) }}">
                        <img class="card-img-top" src="{{ asset('storage/'.$blog->imagen) }}" alt="Card image cap">
                    </a>
                    <div class="card-body">
                        <p class="card-text">
                            @foreach ($blog->categorias as $categoria)
                                <span class="badge badge-primary">{{$categoria->categoria}}</span>
                            @endforeach
                        </p>
                        <h4 class="h4">{{$blog->titulo}}</h4>
                    </div>
                </div>
            @endif
            @endforeach
        </div>
    </div>

    <script>
        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navBlog').addClass('active border rounded');
        });

    </script>
@endsection