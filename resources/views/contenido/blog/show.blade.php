@extends('layouts.app')

@section('title', 'blog')

@section('content')
    <div class="container">
        <div class="form-group row">
            <div class="col-sm-4">
                <img style="width: 25%;" id="inputImgActual" src="{{ asset('storage/'.$blog->imagen) }}" alt="{{$blog->titulo}}">
            </div>
            <label for="inputTitulo" class="col-sm-8 col-form-label">
                <h1 class="h1 text-right">{{$blog->titulo}}</h1>
            </label>
        </div>
        <div>
            <h6 class="h6 text-right">
                @foreach ($blog->categorias as $categoria)
                    <span class="badge badge-primary">{{$categoria->categoria}}</span>
                @endforeach
            </h6>
        </div>
        <div>
            <p>{!! $blog->contenido !!}</p>
        </div>
        <div>
            <p>Creado el {{$blog->created_at}} por {{$blog->empleado->name}}</p>
            @guest
                
            @else
                @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('super-admin'))
                <a class="btn btn-warning" href="{{route('blog.show',$blog->slug)}}"><i class="">@svg("edit_icon", "icon-lg")</i> Editar<span class="sr-only">(current)</span></a>
                @endif
            @endguest
        </div>
    </div>

    <script>
        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navBlog').addClass('active border rounded');
        });

    </script>

@endsection