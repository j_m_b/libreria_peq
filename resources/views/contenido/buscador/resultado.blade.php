@extends('layouts.app')

@section('title', 'resultado')

@section('content')
    <div class="container">
        <div>
            <h3 class="h3">Resultados para: {{$busqueda}}</h3>
        </div>
        <div>
            <div class="card-columns">

                @foreach ($resultados_libros as $libro)

                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img data-titulo="{{$libro->titulo}}" data-precio="{{$libro->precio_venta}}" src="{{ asset('storage/'.$libro->imagen) }}" class="card-img verPortadaLibro" data-toggle="modal" data-target="#portadaLibroModal" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">{{$libro->titulo}}</h5>
                                <p class="card-text">{{$libro->descripcion_valoracion}}</p>
                                <p class="card-text"><small class="text-muted">${{$libro->precio_venta}}</small></p>
                                <a href="#" class="card-link ver_detalle" data-id="{{$libro->id}}" data-toggle="modal" data-target="#detalleLibroModal">detalles</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>            
        </div>
    </div>

    @include('modals.buscador.detalle_libro')
    @include('modals.buscador.portada_libro')


    <script>
        $(document).ready(function() {
        var urlDetalleLibro = "{{ route('libro-detalle.show') }}/";        
            $('.ver_detalle').on('click', function(e){
                e.preventDefault();
                let id = $(this).attr('data-id');

                let titulo = $("#tituloLibroModal");
                let autores = $("#detalle-autores");
                let precio = $(".precio");
                let editorial = $("#detalle-editorial");
                let fecha_publicacion = $("#detalle-fecha_publicacion");
                let descripcion = $("#detalle-descripcion_texto");


                titulo.empty();
                editorial.empty();
                fecha_publicacion.empty();
                descripcion.empty();
                precio.empty();

                $.getJSON(urlDetalleLibro+id, function (data) {
                    titulo.text(data.titulo);
                    editorial.val(data.editorial);
                    fecha_publicacion.val(data.fecha_publicacion);
                    descripcion.text(data.descripcion_valoracion);
                    precio.html(data.precio_venta);

                    let lista_autores = " ";
                    lista_autores += "<ul>";
                    $.each(data.autores, function(index, element){
                        lista_autores += "<li>"+data.autores[index].nombre+" "+data.autores[index].apellido+"</li>";                        
                    });
                    lista_autores += "</ul>";

                    autores.html(lista_autores);
                });
            });

            $('.verPortadaLibro').on('click', function(e){
                let ruta_imagen = $(this).attr('src');
                let titulo = $(this).attr('data-titulo');
                let precio = $(this).attr('data-precio');
                //alert(ruta_imagen);

                let imagenPortada = $("#imagenLibroPortada");

                imagenPortada.empty();
                $("#tituloPortadaLibroModal").empty();
                $(".precio").empty();
                $("#tituloPortadaLibroModal").text(titulo);
                $(".precio").html(precio); 
                imagenPortada.attr('src', ruta_imagen);

            });

        });
    </script>

@endsection