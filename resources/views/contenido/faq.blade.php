@extends('layouts.app')

@section('title', 'faq')

@section('content')
    <div class="container">
        <h2 class="h2 text-center"><strong>Preguntas y respuestas mas frecuentes</strong></h2>
        <div class="accordion" id="accordionExample">
            @foreach ($FAQs as $faq)   
            <div class="card">
                <div class="card-header" id="heading{{$faq->id}}">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">
                        {{$faq->pregunta}}
                        </button>
                    </h2>
                </div>
            
                <div id="collapse{{$faq->id}}" class="collapse" aria-labelledby="heading{{$faq->id}}" data-parent="#accordionExample">
                    <div class="card-body">
                        {{$faq->respuesta}}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <script>
        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navFAQ').addClass('active border rounded');
        });
    
    </script>
@endsection