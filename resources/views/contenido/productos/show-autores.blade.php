@extends('layouts.app')

@section('title', 'productos')

@section('content')
    <div class="container">
        <div>
            <h1 class="h1 text-center">Lista de autores</h1>
        </div>
        <div class="row align-items-center justify-content-center">
            <a href="{{route('productos.libros')}}" class="btn btn-primary align-center">Buscar por libros</a>            
        </div>
        <div class="row align-items-center justify-content-center">
            <table id="autores_lista_disponible-table" class="table display compact nowrap" style="width:100%">
                <thead class="thead-dark">
                    <th></th>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Fecha de nacimiento</th>
                    <th>Fecha de muerte</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <style>
        td.details-control {
            background: url("{{ asset('storage/resources/details_open.png') }}") no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url("{{ asset('storage/resources/details_close.png') }}") no-repeat center center;
        }
    </style>

    <script>
        var urlAutores = "{{ route('producto-autores.table') }}";

        
        function format ( d ) {
            let libros_data = '<table cellpadding="5" cellspacing="0" border="1" style="padding-left:50px;">';
            libros_data += '<tr>';
            libros_data += '<th>Libro</th>';
            libros_data += '<th>Titulo</th>';
            libros_data += '<th>Editorial</th>';
            libros_data += '<th>Publicacion</th>';
            libros_data += '<th>Precio</th>';
            libros_data += '</tr>';
            $.each(d.libros, function(index, element){
                libros_data += '<tr>';
                libros_data += '<td>'+d.libros[index].id+'</td>';
                libros_data += '<td>'+d.libros[index].titulo+'</td>';
                libros_data += '<td>'+d.libros[index].editorial+'</td>';
                libros_data += '<td>'+d.libros[index].fecha_publicacion+'</td>';
                libros_data += '<td>'+d.libros[index].precio_venta+'</td>';
                libros_data += '</tr>';
            });
            libros_data += '</table>';
            return libros_data;
        };

        $(document).ready( function () {

            $(".nav li").removeClass("active border rounded");
            $('.navProductos').addClass('active border rounded');
            
            var table = $('#autores_lista_disponible-table').DataTable({
                "lengthMenu": [[10,30, 50, -1], [10,30, 50, "Todos"]],
                "processing": true,
                "serverSide": true,
                "ajax": ( {
                    url: urlAutores,
                    pages: 10 // number of pages to cache
                }),
                "columns": [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ''
                    },                  
                    {data: 'id', name: 'id'},                    
                    {data: 'nombre', name: 'nombre'},
                    {data: 'apellido', name: 'apellido'},
                    {data: 'fecha_nacimiento', name: 'fecha_nacimiento'},
                    {data: 'fecha_muerte', name: 'fecha_muerte'},

                ]
            });


            // Add event listener for opening and closing details

            $('#autores_lista_disponible-table tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row( tr );
        
                if ( row.child.isShown() ) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    row.child( format(row.data()) ).show();
                    tr.addClass('shown');
                }
            });


        });
    </script>
@endsection