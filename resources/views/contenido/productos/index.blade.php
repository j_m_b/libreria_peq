@extends('layouts.app')

@section('title', 'productos')

@section('content')
    <div class="container">
            <div class="jumbotron">
                <h1 class="display-4 text-center">Productos</h1>
                <p class="lead">Bienvenid@ a nuestra lista de productos, podras encontrar una gran varierdad de libros en nuestra libreria.</p>
                <hr class="my-4">
                <div class="row align-items-center justify-content-center">
                    <div class="card" style="width: 18rem;">
                        <img src="{{ asset('storage/resources/book_productos.png') }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Libros</h5>
                            <p class="card-text">Aqui encontraras la lista de libros y precios disponibles en nuestra libreria. Si no encuentras el libro que deseas puedes <a href="{{route('contactenos')}}">contactarnos</a>.</p>
                            <a href="{{route('productos.libros')}}" class="btn btn-primary">Buscar</a>
                        </div>
                    </div>
                    <div class="card" style="width: 18rem;">
                        <img src="{{ asset('storage/resources/autor_productos.png') }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Autores</h5>
                            <p class="card-text">Tambien puedes realizar la busqueda por el autor, aqui veras la lista de los escritores y sus libros mas los precios.</p>
                            <a href="{{route('productos.autores')}}" class="btn btn-primary">Buscar</a>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <script>
        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navProductos').addClass('active border rounded');
        });

    </script>
    
@endsection