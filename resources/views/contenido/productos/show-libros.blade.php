@extends('layouts.app')

@section('title', 'productos')

@section('content')
    <div class="container">
        <div>
            <h1 class="h1 text-center">Lista de libros</h1>
        </div>
        <div class="row align-items-center justify-content-center">
            <a href="{{route('productos.autores')}}" class="btn btn-primary align-center">Buscar por autores</a>            
        </div>
        <div class="row align-items-center justify-content-center">
            <table id="libros_lista_disponible-table" class="table display compact nowrap" style="width:100%">
                <thead class="thead-dark">
                    <th></th>
                    <th>ID</th>
                    <th>Titulo</th>
                    <th>Editorial</th>
                    <th>Precio ($)</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <style>
            td.details-control {
                background: url("{{ asset('storage/resources/details_open.png') }}") no-repeat center center;
                cursor: pointer;
            }
            tr.shown td.details-control {
                background: url("{{ asset('storage/resources/details_close.png') }}") no-repeat center center;
            }
        </style>
    
        <script>
            var urlLibros = "{{ route('producto-libros.table') }}";
    
            
            function format ( d ) {
                let autor_data = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
                autor_data += '<tr>';
                autor_data += '<th>autores:</th>';
                autor_data += '</tr>';
                $.each(d.autores, function(index, element){
                    autor_data += '<tr>';
                    autor_data += '<td>('+d.autores[index].id+') '+d.autores[index].apellido+' '+d.autores[index].nombre+' ['+d.autores[index].fecha_nacimiento+' - '+d.autores[index].fecha_muerte+']</td>';
                    autor_data += '</tr>';
                });
                autor_data += '</table>';
                return autor_data;
            };
    
            $(document).ready( function () {

                $(".nav li").removeClass("active border rounded");
                $('.navProductos').addClass('active border rounded');

                var table = $('#libros_lista_disponible-table').DataTable({
                    "lengthMenu": [[10,30, 50, -1], [10,30, 50, "Todos"]],
                    "processing": true,
                    "serverSide": true,
                    "ajax": ( {
                        url: urlLibros,
                        pages: 10 // number of pages to cache
                    }),
                    "columns": [
                        {
                            "className":      'details-control',
                            "orderable":      false,
                            "data":           null,
                            "defaultContent": ''
                        },                  
                        {data: 'id', name: 'id'},                    
                        {data: 'titulo', name: 'titulo'},
                        {data: 'editorial', name: 'editorial'},
                        {data: 'precio_venta', name: 'precio_venta'},
                    ]
                });
    
    
                // Add event listener for opening and closing details
    
                $('#libros_lista_disponible-table tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = table.row( tr );
            
                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child( format(row.data()) ).show();
                        tr.addClass('shown');
                    }
                });
    

            });
        </script>
@endsection