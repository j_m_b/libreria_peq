@extends('layouts.app')

@section('title', 'Libros')

@section('content')
    <div class="container">
        <div id="carouselIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselIndicators" data-slide-to="1"></li>
                <li data-target="#carouselIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <h3>
                        Servicio de calidad con precios de calidad
                    </h3>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem consectetur in sit itaque beatae eum obcaecati architecto, asperiores nihil quia id rerum debitis deserunt vel quae dolorum autem ipsa recusandae!</p>

                </div>
                <div class="carousel-item">
                    <h3>
                        Ultimo jueves de cada mes 40% de descuento !APROVECHA¡
                    </h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et reprehenderit quibusdam ipsam dolorem voluptatem praesentium ipsa, consequuntur veniam dicta dolores at quo sapiente debitis id, quasi officia voluptatibus nemo ipsum?</p>
                </div>
                <div class="carousel-item">
                    
                    <h3>
                        Dimicilios gratis
                    </h3>
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eaque necessitatibus quibusdam neque excepturi, ea hic, culpa consequuntur, esse suscipit quo saepe illo non inventore? Odit accusamus assumenda unde molestiae fuga!</p>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="row bg-dark text-white">
            <div class="col-md-6">
                <h2>Ultimos articulos del blog agregads</h2>
                @foreach ($blogs as $blog)
                    <a href="#" class="list-group-item list-group-item-action">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">{{$blog->titulo}}</h5>
                            <small>{{$blog->created_at}}</small>
                        </div>
                        <p>
                            @foreach ($blog->categorias as $categoria)
                            <span class="badge badge-primary">{{$categoria->categoria}}</span>
                            @endforeach
                        </p>
                        <small>{{$blog->empleado->name}}</small>
                    </a>
                @endforeach
            </div>
            <div class="col-md-6">
                <h2>Ultimos libros agregads</h2>
    
                <div class="list-group">
                    @foreach ($libros as $libro)
                    <a href="#" class="list-group-item list-group-item-action">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">{{$libro->titulo}}</h5>
                            <small>{{$libro->created_at}}</small>
                        </div>
                        <p class="mb-1">{{$libro->descripcion_valoracion}}</p>
                        <small>{{$libro->editorial}}</small>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div>
            <h5>Tienes alguna duda o inquietud, puedes <a href="">contactarnos</a> o ingresar a <a href=""> preguntas frecuentes </a></h5>
        </div>
    </div>
@endsection