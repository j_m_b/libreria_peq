@extends('layouts.app')

@section('title', 'acerca-de')

@section('content')
    <div class="container">
        <div>
            <h2 class="h2 text-center"><strong>Acerca de nosotros</strong></h2>
        </div>
        <div class="media position-relative row">
            <div class="col-md-4">
                <img src="{{ asset('storage/resources/my_place_1.jpg') }}" class="img-thumbnail" alt="...">
            </div>
            <div class="media-body col-md-8">
                <h5 class="mt-0"><strong>¿Quienes somos?</strong></h5>
                <p>Lorem ipsum dolor sit amet, duo noster option eligendi id, illud nullam vis ne, vix ut labores mnesarchum. In congue viderer eam, principes accommodare vis no. Te cum magna homero. Vim at probo harum affert, in ius summo probatus. Pri adhuc suscipit te, oblique sanctus invenire ex duo. Ius harum repudiare scribentur cu.</p>
            </div>
        </div>


        <div class="media position-relative row">
            <div class="media-body col-md-6">
                <h5 class="mt-0"><strong>Nuestra mision y vision</strong></h5>
                <p>Cu has mazim explicari consectetuer, nihil ridens utamur ut vis. Munere invidunt prodesset no sed. Mei te nibh feugait. Duo eu omnis diceret lobortis, mei ut nonumy tamquam luptatum. Ei altera intellegat mei.</p>
                <p>Est enim prima altera in. Eum cu falli tamquam reprehendunt. Nostro urbanitas qui in, ea vel omittam instructior. Ut movet verear usu, has eu doming delectus senserit, dissentiet philosophia te per. No ius dicat vitae facilis, ei quot alterum elaboraret eos, prima choro iriure his eu.</p>

            </div>
            <div class="col-md-6">
                <img src="{{ asset('storage/resources/libros_uso.jpg') }}" class="img-thumbnail float-right" alt="...">
            </div>
        </div>


        <h5 class="mt-0"><strong>Promesa de venta</strong></h5>
        <div class="media position-relative row">
            <div class="col-md-2">
                <img src="{{ asset('storage/resources/libro_venta.jpg') }}" class="img-thumbnail" alt="...">
            </div>
            <div class="media-body col-md-10">
                <p>Pro eu congue scaevola elaboraret, porro affert neglegentur ut mel, mea at pericula intellegam. Duo ridens molestie mandamus eu. Eam in hinc suavitate, cum te persecuti dissentiet. Quis verterem iracundia ne vel, ut vis iisque perfecto, decore iuvaret tractatos cum et.</p>
            </div>
        </div>
    </div>



    <script>
        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navAcercaDe').addClass('active border rounded');
        });
    
    </script>
@endsection