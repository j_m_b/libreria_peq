@extends('layouts.app')

@section('title', 'contactenos')

@section('content')
    <div class="container">
        <div>
            <div>
                <h3 class="h3 text-center">Contactenos</h3>
            </div>
            <div>
                <h5 class="h5">Recuerda</h5>
                <ul>
                    <li>Escribir correctamente tus datos</li>
                    <li>Tienes solo 250 caracteres en el cajon de "Mensaje"</li>
                    <li>Estar atento a tu correo electronico, pues alli llegara tu respuesta</li>
                </ul>
            </div>
            <form action="{{route('contactenos.store')}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputNombres">Nombres</label>
                        <input type="text" name="nombres" class="form-control" id="inputNombres" placeholder="Nombres">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputApellidos">Apellidos</label>
                        <input type="text" name="apellidos" class="form-control" id="inputApellidos" placeholder="Apellidos">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Email</label>
                        <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Escribe el correo electronico al cual te responderemos">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputTelefono">Telefono</label>
                        <input type="number" name="telefono" class="form-control" id="inputTelefono" placeholder="Escribe tu numero de contacto">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <label for="inputRespuesta" class="col-sm-2 col-form-label">Mensaje</label>
                        <textarea id="inputRespuesta" maxlength="250" onkeyup="countChar(this)" class="form-control" rows="7" name="pregunta" placeholder="Cuentanos como podemos ayudarte"></textarea>                                                    
                        <div id="charNum"></div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="icon_lg_white">@svg("send_icon", "icon-lg")</i> Enviar</button>
            </form>
        </div>
    </div>


    <script>
        function countChar(val) {
            var len = val.value.length;
            if (len >= 250) {
            val.value = val.value.substring(0, 250);
            } else {
            $('#charNum').text(250 - len);
            }
        };

        $(document).ready( function () {
            $(".nav li").removeClass("active border rounded");
            $('.navContactenos').addClass('active border rounded');
        });
    </script>

@endsection