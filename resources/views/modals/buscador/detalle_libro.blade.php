<div class="modal fade" id="detalleLibroModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="tituloLibroModal"><span class="badge badge-secondary precio"></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div>
                <div class="form-group row">
                    <label for="detalle-autores" class="col-sm-2 col-form-label">Autores:</label>
                    <div class="col-sm-10">
                        <p id="detalle-autores"></p>                        
                    </div>
                </div>
                <div class="form-group row">
                    <label for="detalle-editorial" class="col-sm-2 col-form-label">Editorial:</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="detalle-editorial" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="detalle-fecha_publicacion" class="col-sm-2 col-form-label">Del:</label>
                    <div class="col-sm-10">
                        <input type="date" readonly class="form-control-plaintext" id="detalle-fecha_publicacion" value="">
                    </div>
                </div>

                <p>
                    <a class="" data-toggle="collapse" href="#collapseDescripcion" role="button" aria-expanded="false" aria-controls="collapseDescripcion">
                        Descripcion
                    </a>
                </p>
                <div class="collapse" id="collapseDescripcion">
                    <div class="card card-body">
                        <p id="detalle-descripcion_texto"></p>
                    </div>
                </div>

                
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>