<div class="modal" tabindex="-1" role="dialog" id="revisarMensaje-contactenos">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title titulo-mensaje"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="responder-contactenos-form" action="#" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group row">
                        <label for="nombres" class="col-sm-2">Nombres: </label>
                        <div class="col-sm-10">
                            <p id="nombres"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="telefono" class="col-sm-2">Telefono: </label>
                        <div class="col-sm-10">
                            <p id="telefono"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2">Email: </label>
                        <div class="col-sm-10">
                            <p id="email"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pregunta" class="col-sm-2">Mensaje: </label>
                        <div class="col-sm-10">
                            <p id="pregunta"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="respuesta" class="col-sm-2">Respuesta: </label>
                        <div class="col-sm-10">
                            <p id="respuesta"></p>                            
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="empleado" class="col-sm-2">por: </label>
                        <div class="col-sm-10">
                            <p id="empleado"></p>                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('super-admin'))
                        <button type="submit" class="cambiar_estado btn btn-warning"><i class="">@svg("swap_icon", "icon-lg")</i> Cambiar estado</button>                        
                        <button type="button" id="eliminar_mensaje" class="btn btn-danger" data-id=""><i class="icon_lg_white">@svg("delete_icon", "icon-lg")</i> Eliminar</button>                        
                    @endif
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="icon_lg_white">@svg("close_icon", "icon-lg")</i> Cerrar</button>
                </div>
            </form>
            </div>
        </div>
    </div>