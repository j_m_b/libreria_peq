<div class="modal" tabindex="-1" role="dialog" id="verMensaje-contactenos">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title titulo-mensaje"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="responder-contactenos-form" action="#" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group row">
                        <label for="nombres" class="col-sm-2 col-form-label">Nombres</label>
                        <div class="col-sm-10">
                            <p id="nombres"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="telefono" class="col-sm-2 col-form-label">Telefono</label>
                        <div class="col-sm-10">
                            <p id="telefono"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <p id="email"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pregunta" class="col-sm-2 col-form-label">Mensaje</label>
                        <div class="col-sm-10">
                            <p id="pregunta"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputRespuesta" class="col-sm-2 col-form-label">Respuesta</label>
                        <div class="col-sm-10">
                            <textarea id="inputRespuesta" class="form-control" rows="3" name="respuesta" placeholder="Escribe la respuesta"></textarea>                                                    
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="icon_lg_white">@svg("send_icon", "icon-lg")</i> Responder</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="icon_lg_white">@svg("close_icon", "icon-lg")</i> Cerrar</button>
                </div>
            </form>
            </div>
        </div>
    </div>