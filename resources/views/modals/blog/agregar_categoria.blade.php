<div class="modal" tabindex="-1" role="dialog" id="agregarCategoria">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Agregas categorias</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{ route('blog.categorias.add',$blog->id)}}" method="POST" enctype="multipart/form-data">
        <div class="modal-body">

                {{ csrf_field() }}
                <div class="form-group row">
                    <div class="col-sm-10">
                        @foreach ($categorias as $categoria)
                        <div class="form-check">
                            @if (in_array($categoria->categoria, $categorias_activas))
                            <input class="form-check-input categoria" type="checkbox" name="categorias[]" value="{{$categoria->id}}" checked>
                            @else
                            <input class="form-check-input categoria" type="checkbox" name="categorias[]" value="{{$categoria->id}}">
                            @endif
                            <label class="form-check-label" for="cat_{{$categoria->id}}">{{$categoria->categoria}}</label>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="icon_lg_white">@svg("cancel_icon", "icon-lg")</i> Cancelar</button>
                <button type="submit" class="btn btn-primary"><i class="icon_lg_white">@svg("save_icon", "icon-lg")</i> Guardar</button>
            </div>
        </form>
        </div>
    </div>
</div>