<div class="modal" tabindex="-1" role="dialog" id="crearCategoria">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Crear categoria</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{ route('blog.categorias.store') }}" method="POST" enctype="multipart/form-data">
        <div class="modal-body">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label for="inputCategoria" class="col-sm-2 col-form-label">Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputCategoria" name="categoria" placeholder="Nueva categoria">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="icon_lg_white">@svg("cancel_icon", "icon-lg")</i> Cancelar</button>
                <button type="submit" class="btn btn-primary"><i class="icon_lg_white">@svg("save_icon", "icon-lg")</i> Crear</button>
            </div>
        </form>
        </div>
    </div>
</div>