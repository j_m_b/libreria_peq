<div class="modal" tabindex="-1" role="dialog" id="crearFAQ">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crear FAQ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('faq.store') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <label for="inputPregunta" class="col-sm-2 col-form-label">Pregunta</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputPregunta" name="pregunta" placeholder="¿Cual es la pregunta?">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputRespuesta" class="col-sm-2 col-form-label">Respuesta</label>
                        <div class="col-sm-10">
                            <textarea id="inputRespuesta" class="form-control" rows="3" name="respuesta" placeholder="Escribe la respuesta"></textarea>                                                    
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="icon_lg_white">@svg("cancel_icon", "icon-lg")</i> Cancelar</button>
                    <button type="submit" class="btn btn-primary"><i class="icon_lg_white">@svg("save_icon", "icon-lg")</i> Crear</button>
                </div>
            </form>
            </div>
        </div>
    </div>