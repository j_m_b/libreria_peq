<div id="cambiar-password" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <div class="container">
                    <div class="col-sm-8">
                        <h3 class="h3">Restablecer contraseña</h3>
                    </div>
              </div>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="container">
                <form action="{{route('cuentas.update-password', $cuenta->id)}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="cuenta_id" value="{{$cuenta->id}}">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <label for="inputClave" class="col-sm-2 col-form-label">Contraseña</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control inputData" id="inputClave" name="password" placeholder="Escriba la nueva contraseña">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputClaveComprobacion" class="col-sm-2 col-form-label">Escriba nuevamente la contraseña</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="inputClaveComprobacion" name="password_comprobacion" placeholder="Ingrese nuevamente la contraseña para confirmar">
                        </div>
                    </div>
                    <button id="guardarClave" type="submit" class="btn btn-primary"><i class="icon_lg_white">@svg("save_icon", "icon-lg")</i> Guardar</button>                    
                  </form>
              </div>
          </div>
      </div>
    </div>
  </div>