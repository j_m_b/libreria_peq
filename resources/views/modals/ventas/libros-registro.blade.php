<div id="detalle_registro_compra" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <div class="container">
                        <div class="col-sm-8">
                            <h4 class="h4">Detalles la compra</h4>
                        </div>  
                  </div>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                    <div>
                        <label for="clienteCompra" class="col-sm-4 col-form-label">Cliente: <strong><p id="clienteCompra"></p></strong></label>
                    </div>
                    <div>
                        <label for="registroCompra" class="col-sm-4 col-form-label">Registro: <strong><p id="registroCompra"></p></strong></label>
                        <label for="fechaCompra" class="col-sm-4 col-form-label">Fecha: <strong><p id="fechaCompra"></p></strong></label>
                        <label for="vendedorCompra" class="col-sm-4 col-form-label">Vendedor: <strong><p id="vendedorCompra"></p></strong></label>
                        <label for="totalCompra" class="col-sm-4 col-form-label">Total: <strong><p id="totalCompra"></p></strong></label>
                    </div>
                    <div>
                        <table class="table table-dark">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Libro</th>
                                    <th>Autores</th>
                                    <th>Precio</th>
                                    <th>Cantidad</th>
                                </tr>
                            </thead>
                            <tbody id="librosTablaCompra">
                            </tbody>
                        </table>
                    </div>
              </div>
          </div>
        </div>
    </div>