<div id="lista-clientes" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <div class="container">
                  <div class="col-sm-8">
                  <a class="btn btn-secondary" href="{{route('clientes.create')}}"><i class="icon_lg_white">@svg("add_person_icon", "icon-lg")</i> Crear cliente</a>
                  </div>
              </div>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              @include('compras.clientes_lista-table')
          </div>
      </div>
    </div>
</div>