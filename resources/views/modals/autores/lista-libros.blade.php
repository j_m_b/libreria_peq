<div id="lista-libros" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <div class="container">
                  <div class="col-sm-8">
                  <a class="btn btn-secondary" href="{{route('libros.create')}}"><i class="icon_lg_white">@svg("book", "icon-lg")</i> Ir a libros</a>
                  </div>
              </div>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              @include('autores.libros-table')
          </div>
      </div>
    </div>
  </div>