# composer install
# cp .env.produccion .env
# php artisan key:generate
# php artisan migrate
# php artisan db:seed
# rm public/storage
# php artisan storage:link


sudo docker-compose exec app composer install
sudo docker-compose exec app cp .env.produccion .env
sudo docker-compose exec app php artisan key:generate
sudo docker-compose exec app php artisan migrate
sudo docker-compose exec app php artisan db:seed
sudo docker-compose exec app rm public/storage
sudo docker-compose exec app mkdir -p storage/app/public/resources/
sudo docker-compose exec app cp -r imgs_test/img/* storage/app/public/resources/
sudo docker-compose exec app php artisan storage:link
